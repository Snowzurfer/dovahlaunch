package  
{
	import Collidable_Objects.WoodenPlatformManager;
	import org.flixel.plugin.photonstorm.*;
	import flash.geom.Rectangle;
	import org.flixel.*;
	import flash.utils.getTimer;
	import org.granite.math.BigInteger;
	import FGL.GameTracker.GameTracker;
	//import FGL.GameTracker.GameTrackerErrorEvent;
	
	/**
	 * The play state
	 * @author Snowzurfer
	 */
	public class PlayState extends FlxState
	{
		// External resources
		[Embed(source = "../assets/mainTheme.mp3")] private var themeMusic:Class;
		[Embed(source = "../assets/Hit_Hurt30.mp3")] private var falledSFX:Class;
		
		private var totalTrip:int; // Total lenght percurred
		private var lastY:int; // Used to determine the max lenght percurred
		private var scrollsNumber:int; // How many times the screen has scrolled
		private var highestY:int; // Max height conquerred
		private var fallingTimePF:int; // How long the player falls before to die
		private var lastFallingTimePF:int; // Used with fallingTimePF
		private var coins:int;
		private var activePowerUps:Array;
		
		//Pause Test
		public var paused:PauseState;
		
		private var fallen:Boolean; // True if it's time to die for the player
		
		public function PlayState(activePowerUps:Array, previousCoins:int = 0)
		{
			super();
			this.coins = previousCoins;
			this.activePowerUps = activePowerUps;
		}
		
		override public function create():void
		{
			// Initialize variables
			totalTrip = 0;
			scrollsNumber = 0;
			fallingTimePF = 90;
			lastFallingTimePF = 0;
			fallen = false;
			
			paused = new PauseState();
			
			// Initialize the registry, contaning everything useful
			Registry.init(coins, activePowerUps);
			
			// Add everything to the main game in order to process it
			add(Registry.sky1);
			add(Registry.sky2);
			add(Registry.mountains);
			add(Registry.objectsManager);
			//add(Registry.dragonsReach);
			//add(Registry.platformsManager);
			//add(Registry.fuzManager);
			//add(Registry.dragonManager);
			//add(Registry.arrowsManager);
			add(Registry.ground);
			add(Registry.dummy)
			add(Registry.player);
			add(Registry.hud);
			add(Registry.fx);
            add(paused);
			
			//	These are debugger watches. Bring up the debug console (the ' key by default) and you'll see their values in real-time as you play
			FlxG.watch(Registry.player.velocity, "y", "pvy");
			FlxG.watch(Registry.dummy.velocity, "y", "dvy");
			FlxG.watch(Registry.sky1, "y", "sky1 y");
			FlxG.watch(Registry.player, "y", "pypos");
			FlxG.watch(Registry.sky2, "y", "sly2 y");
			
			//GameTracker.api.beginGame(0, "PlayState", "Finished to create the playstate");
			//GameTracker.api.beginLevel(1, 0, "PlayState", "Start the game");
			
			// Start to play background music
			FlxG.music.fadeIn(1);
			FlxG.playMusic(themeMusic, 1);
		}
		
		override public function update():void
		{
			
			
			if (!paused.showing)
			{
				super.update();
				
				//Something to pause with
				if (FlxG.keys.P)
				{
					paused.showPaused();
					paused = new PauseState;			
					paused.showPaused();
					add(paused);
				}
				
				FlxG.collide(Registry.player, Registry.ground);
				FlxG.collide(Registry.dummy, Registry.ground);
				/*FlxG.overlap(Registry.player, Registry.platformsManager, Registry.player.playerHitObject);
				FlxG.overlap(Registry.player, Registry.fuzManager, Registry.player.playerHitObject);
				FlxG.overlap(Registry.player, Registry.dragonManager, Registry.player.playerHitObject);
				FlxG.overlap(Registry.player, Registry.arrowsManager, Registry.player.playerHitObject);*/
				FlxG.overlap(Registry.player, Registry.objectsManager, Registry.player.playerHitObject);
				
				// Make the dummy follow the player
				Registry.dummy.velocity.y = Registry.player.velocity.y;
				Registry.dummy.acceleration.y = Registry.player.acceleration.y;
				
				// If the player is falling and the screen is scrolling
				if (Registry.player.velocity.y < 0 && Registry.player.y <= 50* 2)
				{
					Registry.player.y = 50* 2; // Stop the player
					// Scroll everything
					Registry.sky1.velocity.y = -Registry.player.velocity.y;
					Registry.sky2.velocity.y = Registry.sky1.velocity.y;
					Registry.ground.velocity.y = Registry.sky1.velocity.y;
					//Registry.dragonsReach.velocity.y = Registry.sky1.velocity.y;
					Registry.mountains.velocity.y = 50* 2;
					
					// If the first sky has finished to scroll, reset it
					if (Registry.sky1.y >= FlxG.height)
					{
						Registry.sky1.y -= (Registry.sky1.height * 2);
						scrollsNumber++;	
					}
					
					// If the second sky has finished to scroll, reset it
					if (Registry.sky2.y >= FlxG.height)
					{
						Registry.sky2.y -= (Registry.sky2.height * 2);
						scrollsNumber++;
					}
					
					// If the player is still going up, update the counters
					if (lastY >= Registry.dummy.y)
					{
						totalTrip += lastY - Registry.dummy.y;
						highestY = Registry.dummy.y;
						Registry.realMaxHeight = 104* 2 - highestY;
					}
					
					// Reset falling time
					lastFallingTimePF = 0;
				}
				
				// If the player is falling, and he can still fall (and not to die)
				else if ( Registry.player.velocity.y > 0 && Registry.player.y >= FlxG.height - 50* 2 && !fallen)
				{				
					// Increase the time the player has been falling
					lastFallingTimePF++;

					// If the time the player can fall is less than the actual time
					if (lastFallingTimePF >= fallingTimePF)
						fallen = true;
					else
					{
						Registry.player.y = FlxG.height - 50* 2; // Stop the player
						// Scroll everything
						Registry.sky1.velocity.y = -Registry.player.velocity.y;
						Registry.sky2.velocity.y = Registry.sky1.velocity.y;
						Registry.mountains.velocity.y = -50* 2;
							
						// If the first sky has finished to scroll, reset it
						if (Registry.sky1.y <= -Registry.sky1.height)
						{
							Registry.sky1.y += Registry.sky1.height * 2;
							scrollsNumber--;
						}
						
						// If the second sky has finished to scroll, reset it
						if (Registry.sky2.y <= -Registry.sky1.height)
						{
							Registry.sky2.y += Registry.sky1.height * 2;
							scrollsNumber--;
						}
					}
				}
				
				// If the player is on the ground or he is floating into the air
				else if ( Registry.player.velocity.y == 0)
				{
					// Stop scrolling
					Registry.sky1.velocity.y = 0;
					Registry.sky2.velocity.y = Registry.sky1.velocity.y;
					Registry.ground.velocity.y = Registry.sky1.velocity.y;
					//Registry.dragonsReach.velocity.y = Registry.sky1.velocity.y;
					Registry.mountains.velocity.y = Registry.sky1.velocity.y;
				}
				
				// Take note of the last position of the dummy
				lastY = Registry.dummy.y;
				
				
				
				// If the player goes off the screen, GAMEOVAH!
				if (Registry.player.y >= FlxG.height + 1)
				{
					Registry.player.velocity.y = 0;
					Registry.player.acceleration.y = 0;
					
					for (var i:int = 0; i < 180* 2; i++)
					{
						// I AM DUMB AND I AM JUST WATIN'
					}
					shakeBetweenScreens();
				}
			}
			else
			{
				paused.update();
			}
		}
		
		// Deallocate everything
		override public function destroy():void
		{
			Registry.destroy();
			
			super.destroy();
		}
		
		/**
		 * Shows an animation before to end the game
		 */
		private function shakeBetweenScreens():void
		{
			FlxG.music.fadeOut(0.3);
			FlxG.shake(0.1, 3);
			FlxG.fade(0xff000000, 0.40, gameOver);
		}
		
		
		/**
		 * End the game
		 */
		private function gameOver():void
		{
			GameTracker.api.endLevel(Registry.score.toNumber(), "PlayState", "The user is dead, going to the GameOver state");
			FlxG.switchState(new GameOverState(Registry.score, Registry.realMaxHeight,Registry.player.getKneeStatus(), Registry.coins));
		}
	}

}