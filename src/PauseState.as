package  
{
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import FGL.GameTracker.GameTracker;
	/**
	 * Shows a pause menu inside the game
	 * @author Snowzurfer
	 */
	public class PauseState extends FlxGroup
	{
		// External resources
		//[Embed(source = "../assets/04B_03__.TTF", fontName = "NES", embedAsCFF = "false")] private var fontNES:Class;
		[Embed(source = "../assets/Pause_Borders(1).png")] private var borderPNG:Class;
		[Embed(source = "../assets/Big_Button_0_16.png")] private var button_0PNG:Class;
		[Embed(source = "../assets/Big_Button_1_16.png")] private var button_1PNG:Class;
		
		/**
		* Background rect for the text
		*/
		private var bg:FlxSprite;

		/**
		* The text field used to display the text
		*/
		private var textField:FlxText;

		/**
		* Use this to tell if dialog is showing on the screen or not.
		*/
		public var showing:Boolean;

		internal var displaying:Boolean;

		/**
		 * Called when dialog is finished (optional)
		 */
		private var finishCallback:Function;
		
		private var border:FlxSprite;
		
		private var newGameButton:FlxButtonPlus;
		private var mainMenuButton:FlxButtonPlus;
		private var instructionsButton:FlxButtonPlus;
		
		public function PauseState() 
		{
			exists = false;
			showing = false;
			displaying = false;
			bg = new FlxSprite(10* 2, 10* 2);
			bg.makeGraphic(FlxG.width - (20* 2), FlxG.height - (20* 2), 0xD9FFFFFF);
			
			border = new FlxSprite(bg.x, bg.y, borderPNG);
			
			textField = new FlxText(bg.x, bg.y +8* 2, 140* 2, "Pause", true);
			textField.setFormat("NES", 16* 2, 0xFF000000, "center");
			
			mainMenuButton = new FlxButtonPlus(bg.x+(20* 2), bg.y+(30* 2), fadeBetweenScreensToMain,null, "Main Menu", 100* 2, 16);
			mainMenuButton.textHighlight.setFormat("NES", 8* 2, 0xFF000000, "center");
			mainMenuButton.textNormal.setFormat("NES", 8* 2, 0xFF000000, "center");
			mainMenuButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			newGameButton = new FlxButtonPlus(bg.x+(20* 2), bg.y+50* 2, startGame, null, "Restart", 100* 2, 16* 2);
			newGameButton.textHighlight.setFormat("NES", 8* 2, 0xFF000000, "center");
			newGameButton.textNormal.setFormat("NES", 8* 2, 0xFF000000, "center");
			newGameButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			instructionsButton = new FlxButtonPlus(bg.x+(20* 2), bg.y+70* 2, fadeBetweenScreensToInstructions, null, "Instructions", 100* 2, 16* 2);
			instructionsButton.textHighlight.setFormat("NES", 8* 2, 0xFF000000, "center");
			instructionsButton.textNormal.setFormat("NES", 8* 2, 0xFF000000, "center");
			instructionsButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			
			add(bg);
			add(textField);
			add(border);
			add(instructionsButton);
			add(mainMenuButton);
			add(newGameButton);
			
			
		}
		
		override public function update():void
		{
			// if the pausestate was called
			if (displaying)
			{
				textField.text = "Pause";
				
				if (FlxG.keys.SPACE)
				{
					this.kill();
					this.exists = false;
					showing = false;
					displaying = false;
					
					if (finishCallback != null) 
						finishCallback();
						
					FlxG.mouse.hide();
				}
				else
				{
					FlxG.mouse.show();
					showing = true;
					displaying = true;
				}
				
				super.update();
			}
		}
		
		public function showPaused():void
		{	
			exists = true;
			displaying = true;
			showing = true;
		}
		
		// Goes back to the menu
		private function goToMainMenu():void
		{
			GameTracker.api.endGame(NaN, "GameOverState", "The player choose to go to the main menu");
			FlxG.mouse.hide();
			Registry.destroy();
			FlxG.switchState(new MenuState);
		}
		
		// Goes to instructions
		private function goToInstr():void
		{
			GameTracker.api.alert("The player choose to go to instructions",FlxG.score,"PauseState");
			FlxG.mouse.hide();
			FlxG.switchState(new InstructionsState(true));
		}
		
		// Goes to settings
		private function goToSettings():void
		{
			GameTracker.api.alert("The player choose to go to settings",FlxG.score,"PauseState");
			FlxG.mouse.hide();
			Registry.destroy();
			FlxG.switchState(new SettingsState(true));
		}
		
		/**
		 * Starts the game again
		 */
		private function startGame():void
		{
			GameTracker.api.alert("The player choose to play again", 0, "GameOverState");
			FlxG.mouse.hide();
			Registry.destroy();
			FlxG.switchState(new PlayState([],0));
		}
		
		/*/**
		 * Called when the dialog is completely finished
		 */
		/*public function set finishCallback(val:Function):void
		{
			finishCallback = val;
		}*/
		
		/**
		 * Shows an animation before to start the game
		 */
		private function fadeBetweenScreensToInstructions():void
		{
			FlxG.flash(0xffffffff, 0.75);
			FlxG.fade(0xff000000, 1, goToInstr);
		}
		
		/**
		 * Shows an animation before to start the game
		 */
		private function fadeBetweenScreensToMain():void
		{
			FlxG.music.fadeOut(0.5);
			FlxG.flash(0xffffffff, 0.75);
			FlxG.fade(0xff000000, 1, goToMainMenu);
		}
	}

}