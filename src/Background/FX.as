package Background 
{
	import org.flixel.FlxEmitter;
	import org.flixel.FlxGroup;
	import org.flixel.FlxPoint;
	import org.flixel.FlxText;
	import org.flixel.FlxG;
	
	/**
	 * Shows graphical effects when necessary
	 * @author Snowzufer
	 */
	public class FX extends FlxGroup
	{
		// External resources
		[Embed(source = "../../assets/04B_03__.TTF", fontName = "NES", embedAsCFF = "false")] private var fontNES:Class;
		
		private var scoreText:FlxText;
		private var fuzBigText:FlxText;
		private var orangePixels:FlxEmitter;
		
		public function FX() 
		{
			super();
			
			orangePixels = new FlxEmitter(0,0,40);
			orangePixels.setSize(2, 2);
			orangePixels.gravity = 200;
			orangePixels.setXSpeed( -50, 50);
			orangePixels.setYSpeed( -30, -60);
			orangePixels.setRotation(0, 0);
			orangePixels.makeParticles(OrangePixel, 40, 0, true, 0);

			
			
			//for (var i:int = 0; i < 40; i++) // Create a pool for the pixel emitters
			//{

			//}
			
			scoreText = new FlxText(0, 0, 20* 2);
			scoreText.setFormat("NES", 8* 2, 0xFFFFFFFF);
			scoreText.exists = false;
			fuzBigText = new FlxText(10* 2, 80* 2, 140* 2, "FUS RO DAH!");
			fuzBigText.setFormat("NES", 16* 2, 0xFFFFFFFF, "center");
			fuzBigText.exists = false;
			
			add(orangePixels);
			add(scoreText);
			add(fuzBigText);
		}
		
		// Shows the score each object gives the plater
		public function showScore(value:String, position:FlxPoint):void
		{
			scoreText.alpha = 1;
			scoreText.text = value;
			scoreText.x = position.x;
			scoreText.y = position.y;
			scoreText.exists = true;
			scoreText.velocity.y = -10;
		}
		
		public function showFuz():void
		{
			FlxG.flash(0xFFFFFFFF, 1);
			fuzBigText.text = "FUS RO DAH!";
			fuzBigText.alpha = 1;
			fuzBigText.exists = true;
		}
		
		public function showDragonSlayer():void
		{	
			FlxG.shake(0.009, 1);
			fuzBigText.text = "DRAGONSLAYER!";
			fuzBigText.alpha = 1;
			fuzBigText.exists = true;
		}
		
		public function hideFuzorDragonText():void
		{
			fuzBigText.exists = false;
		}
		
		public function hideScore():void
		{
			scoreText.velocity.y = 0;
			scoreText.exists = false;
		}
		
		override public function update():void
		{
			super.update();
			
			scoreText.alpha -= 0.009;
			
			if (scoreText.exists)
				fuzBigText.alpha -= 0.009;
		}
		
		public function emitParticles(ax:Number, ay:Number):void
		{
			orangePixels.x = ax;
			orangePixels.y = ay;
			orangePixels.start(true, 1, 0, 10* 2);
		}
	}

}