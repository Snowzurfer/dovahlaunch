package Background 
{
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.geom.Rectangle;

	/**
	 * ...
	 * @author Snowzurfer
	 */
	public class OrangePixel extends Bitmap
	{
		/**
		 * This class is simple a strip of pixels used for when the aliens are shot
		 * It's used in Fx.as as the Class given to the FlxEmitters
		 * 
		 * It could easily be a PNG instead, but I like keeping it here so it's really easy to change the colours/size
		 * and see the results immediately. Once I'm 100% happy with it, I would then usually bake it into a PNG
		 */
		public function OrangePixel()
		{
			super(new BitmapData(16* 2, 2* 2, false, 0x000000));
			
			bitmapData.fillRect(new Rectangle(0, 0, 2* 2, 2* 2), 0xFF006E);
			bitmapData.fillRect(new Rectangle(2* 2, 0, 2* 2, 2* 2), 0xFFFFFF);
			bitmapData.fillRect(new Rectangle(4* 2, 0, 2* 2, 2* 2), 0xFF00DC);
			bitmapData.fillRect(new Rectangle(6* 2, 0, 2* 2, 2* 2), 0xFF0000);
			bitmapData.fillRect(new Rectangle(8* 2, 0, 2* 2, 2* 2), 0xFF7F7F);
			bitmapData.fillRect(new Rectangle(10* 2, 0, 2* 2, 2* 2), 0xFF6A00);
			/*bitmapData.fillRect(new Rectangle(12, 0, 2, 2), 0x00FF40);
			bitmapData.fillRect(new Rectangle(14, 0, 2, 2), 0x00FF80);*/
		}
		
	}

}