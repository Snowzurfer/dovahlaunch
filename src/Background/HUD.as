package Background
{
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	/**
	 * Display informations while playing
	 * @author Snowzurfer
	 */
	public class HUD extends FlxGroup
	{
		
		// External resources
		[Embed(source = "../../assets/Dah 1_16.png")] private var dah_0PNG:Class;
		[Embed(source = "../../assets/Dah 2_16.png")] private var dah_1PNG:Class;
		[Embed(source = "../../assets/Fus 1(1).png")] private var fus_0PNG:Class;
		[Embed(source = "../../assets/Fus 2(1).png")] private var fus_1PNG:Class;
		[Embed(source = "../../assets/Ro 1(1).png")] private var ro_0PNG:Class;
		[Embed(source = "../../assets/Ro 2(1).png")] private var ro_1PNG:Class;
		
		// Displays the current charge level of the first jump
		private var firstJumpPowaBar:FlxBar;
		private var firstJumpPowaBarLabel:FlxText;
		
		private var fus:FlxExtendedSprite;
		private var ro:FlxExtendedSprite;
		private var dah:FlxExtendedSprite;
		
		// Displays the current score
		private var score:FlxText;
		
		// Displays the current max height
		private var height:FlxText;
		
		// Displays the current amount of coins
		private var coins:FlxText;
		
		
		public function HUD(dataOwner:Player) 
		{
			firstJumpPowaBar = new FlxBar(8* 2, 20* 2, FlxBar.FILL_BOTTOM_TO_TOP, 4* 2, 80* 2, dataOwner, "firstJumpPowa", 0,4);
			firstJumpPowaBar.createFilledBar(0xFFE06F8B, 0xFFBE2633, true, 0xFFFFFFFF);
			firstJumpPowaBarLabel = new FlxText(firstJumpPowaBar.x + 8* 2, firstJumpPowaBar.y + 16* 2, 7* 2, "Power");
			firstJumpPowaBarLabel.setFormat("NES", 8* 2, 0xFFFFFFFF);
			
			fus = new FlxExtendedSprite(50* 2, 6* 2);
			fus.loadGraphic(fus_0PNG, false, false, 22* 2, 8* 2);
			
			ro = new FlxExtendedSprite(74* 2, 6* 2);
			ro.loadGraphic(ro_0PNG, false, false, 15* 2, 8* 2);
			
			dah = new FlxExtendedSprite(92* 2, 6* 2);
			dah.loadGraphic(dah_0PNG, false, false, 17* 2, 8* 2);
			
			score = new FlxText(2* 2, 2* 2, 50* 2, Registry.score.toString(10));
			score.setFormat("NES", 8* 2);
			score.color = 0xffffffff;
			score.scrollFactor.x = 0;
			score.scrollFactor.y = 0;
			
			height = new FlxText(FlxG.width - 50* 2, 2* 2, 50* 2, Registry.realMaxHeight.toString());
			height.setFormat("NES", 8* 2);
			height.color = 0xffffffff;
			height.scrollFactor.x = 0;
			height.scrollFactor.y = 0;
			
			coins = new FlxText(FlxG.width - 50* 2, 205, 50* 2, Registry.coins.toString(10));
			coins.setFormat("NES", 8* 2);
			coins.color = 0xffffffff;
			coins.scrollFactor.x = 0;
			coins.scrollFactor.y = 0;
			
			add(firstJumpPowaBar);
			add(firstJumpPowaBarLabel);
			add(fus);
			add(ro);
			add(dah);
			add(score);
			add(height);
			add(coins);
			
		}
		
		override public function update():void
		{
			super.update();
			
			score.text = "Score: "+Registry.score.toString(10);
			height.text = "Height:  " +Registry.realMaxHeight.toString(10);
			coins.text = "Coins:  "+Registry.coins.toString(10);
			
		}
		
		// Kill the first jump power level bar
		public function deactivateJumpPowaBar():void
		{
			firstJumpPowaBar.kill();
			firstJumpPowaBarLabel.kill();
		}
		
		public function changeFuzLevel(fuzLevel:int):void
		{
			switch(fuzLevel)
			{
				case 0: // Reset everything
					fus.loadGraphic(fus_0PNG, false, false, 22* 2, 8* 2);
					ro.loadGraphic(ro_0PNG, false, false, 15* 2, 8* 2);
					dah.loadGraphic(dah_0PNG, false, false, 17* 2, 8* 2);
					break;
					
				case 1:
					fus.loadGraphic(fus_1PNG, false, false, 22* 2, 8* 2);
					break;
					
				case 2:
					fus.loadGraphic(fus_1PNG, false, false, 22* 2, 8* 2);
					ro.loadGraphic(ro_1PNG, false, false, 15* 2, 8* 2);
					break;
					
				case 3:
					fus.loadGraphic(fus_1PNG, false, false, 22* 2, 8* 2);
					ro.loadGraphic(ro_1PNG, false, false, 15* 2, 8* 2);
					dah.loadGraphic(dah_1PNG, false, false, 17* 2, 8* 2);
					break;
			}
		}
	}

}