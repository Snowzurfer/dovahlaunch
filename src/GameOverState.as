package  
{
	import org.flixel.*;
	import org.granite.math.BigInteger;
	import FGL.GameTracker.*;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	
	/**
	 * Game Over State
	 * @author Snowzurfer
	 */
	public class GameOverState extends FlxState
	{
		//[Embed(source = "../assets/04B_03__.TTF", fontName = "NES", embedAsCFF = "false")] private var fontNES:Class;
		[Embed(source = "../assets/GameOva_Button_1.png")] private var button_0PNG:Class;
		[Embed(source = "../assets/GameOva_Button_0.png")] private var button_1PNG:Class;
		[Embed(source="../assets/GameOverMusicTheme.mp3")] private var endGameSFX:Class;
		
		private var gameOverText:FlxText;
		private var newGameButton:FlxButtonPlus;
		private var mainMenuButton:FlxButtonPlus;
		private var shopButton:FlxButtonPlus;
		private var moreGamesButton:FlxButtonPlus;
		private var maxHeightText:FlxText;
		private var scoreText:FlxText;
		private var coins:int;
		private var activePowerups:Array;
		
		public function GameOverState(score:BigInteger, maxHeight:int, arrowDeath:Boolean, coins:int) 
		{
			super();
			
			activePowerups = new Array(8);
			activePowerups[0] = 0;
			
			Registry.init(coins,activePowerups);
			
			Registry.sky1.y = -Registry.sky1.height + FlxG.height;
			
			FlxG.playMusic(endGameSFX, 1);
			FlxG.mouse.show();
			FlxG.mouse.screenY = FlxG.mouse.screenX = 0;
			
			if (!arrowDeath) // If the player did not die because of an arrow
			{
				gameOverText = new FlxText(0, 8, 320, "There ain't no water down here.");
				gameOverText.setFormat("NES", 32, 0xffffff, "center");
			}
			else // If the player died because of an arrow
			{
				gameOverText = new FlxText(0, 0, 320, "You took an arrow to the knee.");
				gameOverText.setFormat("NES", 32, 0xffffff, "center");
			}
			
			scoreText = new FlxText(0, 100, FlxG.width, "Your score: "+score.toString(), true);
			scoreText.setFormat("NES", 16, 0xffffff, "center");
			
			maxHeightText = new FlxText(0, 124, FlxG.width, "Your height: " + maxHeight.toString(), true);
			maxHeightText.setFormat("NES", 16, 0xffffff, "center");
			
			newGameButton = new FlxButtonPlus(10 , 77* 2, fadeBetweenScreens, ["newGame"], "New Game", 145, 23);
			newGameButton.textHighlight.setFormat("NES", 8* 2, 0xffffff, "center");
			newGameButton.textNormal.setFormat("NES", 8* 2, 0xffffff, "center");
			newGameButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			mainMenuButton = new FlxButtonPlus(10, 95* 2, fadeBetweenScreens, ["mainMenu"], "Main Menu", 145, 23);
			mainMenuButton.textHighlight.setFormat("NES", 8* 2, 0xffffff, "center");
			mainMenuButton.textNormal.setFormat("NES", 8*2, 0xffffff, "center");
			mainMenuButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			shopButton = new FlxButtonPlus(mainMenuButton.x + mainMenuButton.width + 10, 95 * 2, fadeBetweenScreens, ["shop"], "Shop Items", 145, 23);
			shopButton.textHighlight.setFormat("NES", 8* 2, 0xffffff, "center");
			shopButton.textNormal.setFormat("NES", 8*2, 0xffffff, "center");
			shopButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			moreGamesButton = new FlxButtonPlus(mainMenuButton.x + mainMenuButton.width + 10, 77 * 2, fadeBetweenScreens, null, "More Games", 145, 23);
			moreGamesButton.textHighlight.setFormat("NES", 8* 2, 0xffffff, "center");
			moreGamesButton.textNormal.setFormat("NES", 8*2, 0xffffff, "center");
			moreGamesButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			this.coins = coins;
				
			add(Registry.ground);
            add(gameOverText);
			add(scoreText);
			add(maxHeightText);
			add(newGameButton);
			add(mainMenuButton);
			add(shopButton);
			add(moreGamesButton);
		}
		
		/**
		 * Shows an animation before to start the game
		 */
		private function fadeBetweenScreensToNew():void
		{
			//FlxG.mouse.hide();
			FlxG.flash(0xffffffff, 0.75);
			FlxG.fade(0xff000000, 1, startGame);
		}
		
		/**
		 * Shows an animation before to switch to another state
		 */
		private function fadeBetweenScreens(toWhere:String):void
		{
			//FlxG.mouse.hide();
			FlxG.flash(0xffffffff, 0.75);
			
			// If the player wnats to go to the main menu
			if(toWhere == "mainMenu"){
				FlxG.fade(0xff000000, 1, goToMainMenu);
			}
			else if(toWhere == "newGame"){
				FlxG.fade(0xff000000, 1, startGame);
			}
			else if(toWhere == "shop"){
				FlxG.fade(0xff000000, 1, goToShop);
			}
			else if(toWhere == "moreGames"){
				FlxG.fade(0xff000000, 1, goToMainMenu);
			}
		}
		
		/**
		 * Goes to the main menu
		 */
		private function goToMainMenu():void
		{
			GameTracker.api.endGame(NaN, "GameOverState", "The player choose to go to the main menu");
			FlxG.mouse.hide();
			FlxG.switchState(new MenuState);
		}
		
		/**
		 * Starts the game
		 */
		private function startGame():void
		{
			GameTracker.api.alert("The player choose to play again", 0, "GameOverState");
			FlxG.mouse.hide();
			Registry.destroy();
			FlxG.switchState(new PlayState(activePowerups,coins));
		}
		
		/**
		 * Switch to the shop state
		 */
		private function goToShop():void {
			GameTracker.api.alert("The player went shopping", 0, "GameOverState");
			FlxG.switchState(new ShopPage(coins));
		}
		
		/**
		 * Navigate to the url specified
		 * @param	event
		 */
		private function goToMyURL():void
		{
			navigateToURL(new URLRequest("http://"));
		}
		
	}

}