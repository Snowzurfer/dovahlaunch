package  
{
	import Collidable_Objects.ArrowsManager;
	import Collidable_Objects.DragonManager;
	import Collidable_Objects.FuzManager;
	import Collidable_Objects.ObjectsManager;
	import Collidable_Objects.WoodenPlatformManager;
	import flash.display.PixelSnapping;
	import flash.net.FileReferenceList;
	import org.flixel.*;
	import Background.*;
	import org.granite.math.*;
	
	/**
	 * Manages the objects of the game at an high level
	 * @author Snowzurfer
	 */
	public class Registry 
	{
		// The player
		public static var player:Player;
		
		/*// Platforms manager
		public static var platformsManager:WoodenPlatformManager;
		
		// FUS-RO-DAH manager
		public static var fuzManager:FuzManager;
		
		// Dragons manager
		public static var dragonManager:DragonManager;
		
		// Arrows manager
		public static var arrowsManager:ArrowsManager;*/
		
		public static var objectsManager:ObjectsManager;
		
		// Dummy sprite used to determine the total height
		public static var dummy:FlxSprite;
		
		// Display the data
		public static var hud:HUD;
		
		// Display GFX
		public static var fx:FX;
		
		
		
		
		
		
		

		// External resources
		[Embed(source = "../assets/Immagine(1).png")] public static var dummySkeleton:Class;
		[Embed(source="../assets/Sky_Background_Test(1).png")]public static var skyTilePNG:Class;
		[Embed(source = "../assets/mapCSV_THEONLYLEVEL_Sky.csv", mimeType = "application/octet-stream")] public static var skyCSV:Class;
		[Embed(source = "../assets/mapCSV_THEONLYLEVEL_Map2.csv", mimeType = "application/octet-stream")] public static var groundCSV:Class;
		//[Embed(source="../assets/Dragonsreach.png")] public static var dragonsReachPNG:Class;
		[Embed(source = "../assets/Mountains background 2.png")] public static var mountainsPNG:Class;
		[Embed(source = "../assets/Dovahkiin_SpriteSheet(1).png")] public static var playerMovementPNG:Class;
		
		// The two tilemaps plus the ground
		public static var sky1:FlxTilemap;
		public static var sky2:FlxTilemap;
		public static var ground:FlxTilemap;
		public static var dragonsReach:FlxSprite;
		public static var mountains:FlxSprite;
		
		public static var realMaxHeight:int; // Used to tell the user how high he has gone 
		public static var score:BigInteger;
		public static var scoreGiven:BigInteger; // The score the object gives the player when hitten
		public static var activePowerups:Array; // The powerups active at the moment
		
		// Number of coins collected
		public static var coins:int;
		
		
		public static function init(previousCoins:int, activePowerUpsPassed:Array):void
		{
			activePowerups = activePowerUpsPassed;
			
			// Setting the world properties
			FlxG.worldBounds = new FlxRect(0, 0, FlxG.width, FlxG.height);
			
			// Create the player
			player = new Player(FlxG.width / 2, FlxG.height - 16* 2);
			
			// Create the dummy used to determine the max height
			dummy = new FlxSprite(player.x + 16* 2, player.y, dummySkeleton);
			dummy.visible = false;
			dummy.maxVelocity.y = player.maxVelocity.y;
			dummy.acceleration.y = player.acceleration.y;
			
			realMaxHeight = 0;
			score = new BigInteger("0");
			scoreGiven = new BigInteger("10");
			coins = previousCoins;
			
			
			// Set up the Sky 1
			sky1 = new FlxTilemap();
			sky1.loadMap(new skyCSV, skyTilePNG, 8* 2, 8* 2);
			sky1.setTileProperties(2, FlxObject.NONE);
			sky1.setTileProperties(3, FlxObject.NONE);
			sky1.setTileProperties(1, FlxObject.NONE);
			sky1.setTileProperties(4, FlxObject.NONE);
			sky1.setTileProperties(8, FlxObject.NONE);
			sky1.y = -sky1.height + FlxG.height;
			
			
			// Set up the Sky 2
			sky2 = new FlxTilemap();
			sky2.loadMap(new skyCSV, skyTilePNG, 8* 2, 8* 2);
			sky2.setTileProperties(2, FlxObject.NONE);
			sky2.setTileProperties(3, FlxObject.NONE);
			sky2.setTileProperties(1, FlxObject.NONE);
			sky2.setTileProperties(4, FlxObject.NONE);
			sky2.setTileProperties(8, FlxObject.NONE);
			sky2.y = sky1.y - sky2.height;
			
			// Set up the Ground
			ground = new FlxTilemap();
			ground.loadMap(new groundCSV, skyTilePNG, 8* 2, 8* 2);
			ground.setTileProperties(5, FlxObject.ANY);
			ground.y = sky1.y;
			
			// Set up the mountains background
			mountains = new FlxSprite(0, FlxG.height - (50 * 2), mountainsPNG);
			
			// Create the objects pool
			/*platformsManager = new WoodenPlatformManager(sky1.y);
			dragonManager = new DragonManager(sky1.y);
			fuzManager = new FuzManager(sky1.y);
			arrowsManager = new ArrowsManager(sky1.y);
			*/
			
			objectsManager = new ObjectsManager(sky1.y);
			
			fx = new FX();
			hud = new HUD(player);
		}
		
		// Deallocate everything
		public static function destroy():void 
		{
			player = null;
			dummy = null;

			sky1 = null;
			sky2 = null;
			ground = null;
			dragonsReach = null;
			mountains = null;
			
			//platformsManager = null;
			score = null;
			scoreGiven = null;
			coins = 0;
			activePowerups = null;
			//fuzManager = null;
			
			//rrowsManager = null;
			
			hud = null;
			fx = null;
		}
	}

}