package  
{
	import Collidable_Objects.Dragon;
	import flash.display.Sprite;
	import flash.errors.InvalidSWFError;
	import flash.media.Sound;
	import flash.sampler.NewObjectSample;
	import flash.system.IMEConversionMode;
	import org.flixel.*;
	import FGL.GameTracker.GameTracker;
	import org.flixel.plugin.photonstorm.FlxBitmapFont;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import org.granite.math.BigInteger;
	
	/**
	 * The main menu of the game
	 * @author Snowzurfer
	 */
	public class MenuState extends FlxState
	{
		private var mainTitle:FlxText;
		private var startButton:FlxButton;
		private var copyright:FlxText;
		private var fromInstructions:Boolean;
		private var settingsButton:FlxButton;
		private var instructionsButton:FlxButton;
		private var gametracker:GameTracker;
		private var dragon:FlxSprite;
		private var cloud1:FlxSprite;
		private var cloud2:FlxSprite;
		private var cloud3:FlxSprite;
		private var animatedTitle:FlxSprite;
		//private var bmpFont:FlxBitmapFont;
		
		//[Embed(source="../assets/Commodore Pixelized v1.2.ttf")]
		//[Embed(source = "../assets/uni05_53.ttf", fontName = "", embedAsCFF = "false")] private var font:Class;
		//[Embed(source="../assets/fontNES_0.png")] private var nesBMPFont:Class;
		[Embed(source = "../assets/Button 2_16.png")] private var buttonPNG:Class;
		[Embed(source = "../assets/arrow_to_the_knee_cursor_16.png")] private var cursorPNG:Class;
		[Embed(source = "../assets/littleCloud(1).png")] private var littleCloudPNG:Class;
		[Embed(source = "../assets/Dragon_Sheet_Red(1).png")] private var dragonPNG:Class;
		[Embed(source = "../assets/menuMainTheme.mp3")] private var musicTheme:Class;
		//[Embed(source = "../assets/Main_Title_Container_0.png")] private var animatedTitlePNG_0:Class;
		[Embed(source = "../assets/Main_Title_Container.png")] private var animatedTitlePNG:Class;
		
		
		override public function MenuState(fromInstructions:Boolean = false):void
		{
			super();
			
			this.fromInstructions = fromInstructions;
			
			if (!fromInstructions)
				gametracker = new GameTracker();
			
			GameTracker.api.alert("Inside the menu", 0, "MenuState");
		
			
			mainTitle = new FlxText(0, 10, FlxG.width, "Dovahlaunch");
			mainTitle.setFormat("NES", 8 * 5, 0xFFFFFFFF, "center");
			
			animatedTitle = new FlxSprite(10, 5);
			animatedTitle.loadGraphic(animatedTitlePNG, true, false, 300, 50);
			animatedTitle.addAnimation("rotate", [0, 1], 2);
			
			startButton = new FlxButton(30* 2, 50* 2, "", fadeBetweenScreens);
			startButton.loadGraphic(buttonPNG, true, false, 200, 32);
			startButton.label=new FlxText(0, 0, 200, "Begin", true);
			startButton.label.setFormat("NES", 8*2, 0xFFFFFFFF, "center");
			
			instructionsButton = new FlxButton(30 * 2, 70 * 2, "Instructions", switchToInstructions);
			instructionsButton.loadGraphic(buttonPNG, true, false, 100 * 2, 16 * 2);
			instructionsButton.label =new FlxText(0, 5, 200, "Instructions", true);
			instructionsButton.label.setFormat("NES", 16, 0xFFFFFFFF, "center");
			
			settingsButton = new FlxButton(30* 2, 90* 2, "", switchToSettings)
			settingsButton.loadGraphic(buttonPNG, true, false, 100* 2, 16* 2);
			settingsButton.label = new FlxText(0, 5, 200, "Settings", true);
			settingsButton.label.setFormat("NES", 16, 0xFFFFFFFF, "center");
			
			dragon = new FlxSprite(2* 2,30* 2);
			dragon.loadGraphic(dragonPNG, true, true, 16* 2, 9* 2);
			dragon.maxVelocity.x = 20* 2;
			dragon.addAnimation("fly", [0, 1], 2, true);
			
			cloud1 = new FlxSprite(30* 2, 34* 2, littleCloudPNG);
			cloud1.maxVelocity.x = 10* 2;
			
			cloud2 = new FlxSprite(120* 2, 36* 2, littleCloudPNG);
			cloud2.maxVelocity.x = 10* 2;
			
			cloud3 = new FlxSprite(0* 2, 28* 2, littleCloudPNG);
			cloud3.maxVelocity.x = 10* 2;
			
			copyright = new FlxText(0, 110* 2, 160* 2, "Copyright MMXII, Astrolab Games");
			copyright.setFormat("NES", 8* 2, 0xFFFFFFFF, "center");
			
			add(animatedTitle);
			add(mainTitle);
			add(cloud3)
			add(dragon);
			add(cloud1);
			add(cloud2);
			add(settingsButton);
			add(startButton);
			add(instructionsButton);
			add(copyright);
			FlxG.mouse.show(cursorPNG, 2, 0, 0);
		}
		
		override public function create():void
		{
			FlxG.bgColor = 0xff0094FF;
			
			if (!fromInstructions)
				// Here is where the music is supposed to start
				FlxG.playMusic(musicTheme, 1);
		}
		
		private function switchToSettings():void
		{
			GameTracker.api.alert("Player Clicked on Settings", 0, "MenuState");
			FlxG.switchState(new SettingsState(false));
		}
		
		private function switchToInstructions():void
		{
			GameTracker.api.alert("Player Clicked on Instructions", 0, "MenuState");
			FlxG.switchState(new InstructionsState(false));
		}
		
		override public function update():void
		{
			super.update();
			
			// Animate the title
			animatedTitle.play("rotate");
			
			cloud1.velocity.x = cloud3.velocity.x = 10* 2;
			cloud2.velocity.x = -10* 2;
			
			// Pretty simple, makes the dragon "fly" arounf the level
			if (dragon.facing == FlxObject.RIGHT)
				dragon.velocity.x += dragon.maxVelocity.x;
			else if (dragon.facing == FlxObject.LEFT)
				dragon.velocity.x -= dragon.maxVelocity.x;
			
			if (cloud1.x >= FlxG.width)
				cloud1.x = -cloud1.width;
			if (cloud2.x + cloud2.width <= 0)
				cloud2.x = FlxG.width;
			if (cloud3.x >= FlxG.width)
				cloud3.x = -cloud3.width;
				
			if (dragon.x <= 0 - 16* 2)
			{
				dragon.facing = FlxObject.RIGHT;
			}
			else if (dragon.x >= FlxG.width)
			{
				dragon.facing = FlxObject.LEFT;
			}
			
			dragon.play("fly");
		}
		
		/**
		 * Shows an animation before to start the game
		 */
		private function fadeBetweenScreens():void
		{
			FlxG.music.fadeOut(0.5);
			GameTracker.api.alert("User clicked onto the begin button", 0, "MenuState");
			FlxG.flash(0xffffffff, 0.75);
			FlxG.fade(0xff000000, 1, startGame);
		}
		
		
		/**
		 * Starts the game
		 */
		private function startGame():void
		{
			var dummyArray:Array = new Array(8);
			dummyArray[0] = 0;
			FlxG.mouse.hide();
			FlxG.switchState(new PlayState(dummyArray, 0));
		}
	}

}