package 
{
	
	import flash.display.Sprite;
	import flash.events.Event;
	//import mochi.as3.*;
	
	
	//[SWF(width="640", height="480", backgroundColor="#000000")]
	[Frame(factoryClass = "Preloader")]

	
	/**
	 * Classic main class
	 * @author Snowzurfer
	 */
	dynamic public class Main extends Sprite
	{
		public function Main():void 
		{
			if (stage)
			{
				init();
			}
			else
			{
				addEventListener(Event.ADDED_TO_STAGE, init);
			}
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			var game:DovahLaunch = new DovahLaunch();
			
			addChild(game);
		}
		
	}

}