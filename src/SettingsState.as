package  
{
	import org.flixel.FlxButton;
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import org.flixel.FlxG;
	
	/**
	 * Settings
	 * @author Snowzurfer
	 */
	public class SettingsState extends FlxState
	{
		// External resources
		//[Embed(source = "../assets/04B_03__.TTF", fontName = "NES", embedAsCFF = "false")] private var fontNES:Class;
		[Embed(source = "../assets/Mountains background 2.png")] private var mountainsPNG:Class;
		[Embed(source = "../assets/Button Music_16.png")] private var musicButtonPNG:Class;
		[Embed(source = "../assets/Button 2_16.png")] private var backButtonPNG:Class;
			
		private var settingsLabel:FlxText;
		private var musicText:FlxText;
		private var musicButton:FlxButton;
		private var backButton:FlxButton;
		private var background:FlxSprite;
		
		private var fromPause:Boolean;
		
		public function SettingsState(fromPause:Boolean) 
		{
			super();
			
			this.fromPause = fromPause;
			
			// Simply a label telling you are into settings
			settingsLabel = new FlxText(30* 2, 5* 2, 100* 2, "Settings", true);
			settingsLabel.setFormat("NES", 16* 2, 0xffFFFFFF, "center");
			
			// Button for toggling the music
			musicButton = new FlxButton(20* 2, 45* 2, null, toggleMusic);
			musicButton.loadGraphic(musicButtonPNG, true, false, 19* 2, 16* 2);
			// Text for explaining what the button does
			musicText = new FlxText(50* 2, 48* 2, 100* 2, "Toggle Music On/Off", true);
			musicText.setFormat("NES", 8);
			
			// Button for going back to main menu
			backButton = new FlxButton(30* 2, 100* 2, "Back", backToMenu);
			backButton.loadGraphic(backButtonPNG, true, false, 100* 2, 16* 2);
			backButton.label.setFormat("NES", 8* 2, 0xffFFFFFF, "center");
			
			
			
			// Used for making the instructions more fancy
			background = new FlxSprite(0, FlxG.height - 50*2, mountainsPNG);
			
			add(background);
			add(musicButton);
			add(backButton);
			add(musicText);
			add(settingsLabel);
			
		}
		
		
		private function backToMenu():void
		{
			if(fromPause)
				FlxG.switchState(new PlayState([],0));
			else
				FlxG.switchState(new MenuState(true));
		}
		
		private function toggleMusic():void
		{
			FlxG.mute = !FlxG.mute;
		}
	}

}