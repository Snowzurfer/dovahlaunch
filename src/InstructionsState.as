package  
{
	import Collidable_Objects.Dragon;
	import Collidable_Objects.Fusrodah;
	import Collidable_Objects.WoodenPlatform;
	import Collidable_Objects.WoodenPlatformManager;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import org.flixel.plugin.photonstorm.FlxBar;
	
	/**
	 * Explain how to play the game
	 * @author Snowzurfer
	 */
	public class InstructionsState extends FlxState
	{
		// External resources
		//[Embed(source = "../assets/04B_03__.TTF", fontName = "NES", embedAsCFF = "false")] private var fontNES:Class;
		[Embed(source = "../assets/Mountains background 2.png")] private var mountainsPNG:Class;
		[Embed(source = "../assets/Small_Button_0(1).png")] private var button_0PNG:Class;
		[Embed(source = "../assets/Small_Button_1(1).png")] private var button_1PNG:Class;
		[Embed(source = "../assets/platform_0(1).png")] private var platformPNG:Class;
		[Embed(source = "../assets/Dragon_Sheet_Green(1).png")] private var dragonPNG:Class;
		[Embed(source = "../assets/FuzPowerUp_16.png")] private var fuzPNG:Class;
		[Embed(source = "../assets/arrow_to_the_knee_16.png")] private var arrowPNG:Class;
		
		
		private var firstJumpPowaBar:FlxBar;
		private var firstJumpPowaBarLabel:FlxText;
		
		private var arrow:FlxSprite;
		private var platform_0:FlxSprite;
		private var platform_1:FlxSprite;
		private var platform_2:FlxSprite;
		private var dragon:FlxSprite;
		private var fuz_0:FlxSprite;
		private var fuz_1:FlxSprite;
		private var fuz_2:FlxSprite;
		private var invisiblePlayer:Player;
		private var textShown:FlxText;
		private var nextButton:FlxButtonPlus;
		private var menuButton:FlxButtonPlus;
		private var background:FlxSprite;
		private var content_0:String;
		private var content_1:String;
		private var content_2:String;
		private var content_3:String;
		private var content_4:String;
		
		private var page1:FlxGroup;
		private var page2:FlxGroup;
		private var page3:FlxGroup;
		private var page4:FlxGroup;
		private var page5:FlxGroup;
		
		private var pageCounter:int;
		
		private var fromPause:Boolean;
		
		public function InstructionsState(fromPause:Boolean) 
		{
			
			super();
			
			pageCounter = 1;
			
			this.fromPause = fromPause;
			
			// Button for proceeding to the next instructions page
			nextButton = new FlxButtonPlus(FlxG.width - 60* 2, 100* 2, nextPage, null, "Next", 50* 2, 16* 2);
			nextButton.textHighlight.setFormat("NES", 8* 2, 0xffFFFFFF, "center");
			nextButton.textNormal.setFormat("NES", 8* 2, 0xffFFFFFF, "center");
			nextButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			
			// Button for going back to the main menu
			menuButton = new FlxButtonPlus(10* 2, 100* 2, backToMenu, null, "Menu", 50* 2, 16* 2);
			menuButton.textHighlight.setFormat("NES", 8* 2, 0xffFFFFFF, "center");
			menuButton.textNormal.setFormat("NES", 8* 2, 0xffFFFFFF, "center");
			menuButton.loadGraphic(new FlxSprite(0,0,button_0PNG), new FlxSprite(0,0,button_1PNG));
		
			// Creation of the various content which will be displayed
			content_0 = new String("When you are on the ground, click to jump. The more the bar is filled, the more you will jump!");
			content_1 = new String("Jump onto platforms or dragons to continue to go up.");
			content_2 = new String("Collect the FUS-RO-DAH powerups. When you have 3 of them, the upper FUS-RO-DAH indicator will be totally blue. Click for an extra powerful boost!");
			content_3 = new String("Dodge arrows! If one catches you, you will die.");
			content_4 = new String("Press P to pause the game while playing.                          Press SPACE for going back to game.");
			
			// The text shown on each page
			textShown = new FlxText(50* 2, 25* 2, 100* 2, content_0, true);
			textShown.setFormat("NES", 8* 2, 0xFFFFFFFF, "right");
			
			// Used only for displaying the fuzJumpPowaBar
			invisiblePlayer = new Player(0, 0);
			invisiblePlayer.visible = false;
			invisiblePlayer.acceleration.y = 0;
			
			// Used for making the instructions more fancy
			firstJumpPowaBar = new FlxBar(8* 2, 10* 2, FlxBar.FILL_BOTTOM_TO_TOP, 4* 2, 80* 2, invisiblePlayer, "firstJumpPowa", 0,4* 2);
			firstJumpPowaBar.createFilledBar(0xFFE06F8B, 0xFFBE2633, true, 0xFFFFFFFF);
			firstJumpPowaBarLabel = new FlxText(firstJumpPowaBar.x + 8* 2, firstJumpPowaBar.y + 16* 2, 7* 2, "Power");
			firstJumpPowaBarLabel.setFormat("NES", 8* 2, 0xFFFFFFFF);
			
			// Used for making the instructions more fancy
			background = new FlxSprite(0, FlxG.height - 50* 2, mountainsPNG);
			
			// -First page creation ------------------------------------------------------------------------------------------------------
			page1 = new FlxGroup();
			page1.add(invisiblePlayer);
			page1.add(textShown); // The first page text is immediately loaded, next ones will be loaded later
			page1.add(firstJumpPowaBar);
			page1.add(firstJumpPowaBarLabel);
			// ---------------------------------------------------------------------------------------------------------------------------
			
			// Used for making the instructions more fancy
			dragon = new FlxSprite(20* 2, 40* 2);
			dragon.loadGraphic(dragonPNG, true, true, 16* 2, 9* 2, false);
			dragon.facing = FlxObject.LEFT;
			dragon.addAnimation("fly", [0, 1], 1, true);
			
			// Used for making the instructions more fancy
			platform_0 = new FlxSprite(30* 2, 70* 2, platformPNG);
			platform_1 = new FlxSprite(15* 2, 20* 2, platformPNG);
			platform_2 = new FlxSprite(25* 2, 90* 2, platformPNG);
			
			// Used for making the instructions more fancy
			fuz_0 = new FlxSprite(35* 2, 50* 2, fuzPNG);
			fuz_1 = new FlxSprite(20* 2, 80* 2, fuzPNG);
			fuz_2 = new FlxSprite(10* 2, 20* 2, fuzPNG);
			
			arrow = new FlxSprite(-10* 2, 70* 2, arrowPNG);
			arrow.maxVelocity.x = 50* 2;
			
			page2 = new FlxGroup();
			page3 = new FlxGroup();
			page4 = new FlxGroup();
			page5 = new FlxGroup();
			
			add(background);
			add(page1);
			add(nextButton);
			add(menuButton);
			
			FlxG.mouse.show();
		}
		
		private function nextPage():void
		{
			switch(pageCounter)
			{
				case 0:
					page2.exists = page3.exists = page4.exists = page5.exists = false;
					textShown.text = content_0;
					page1 = new FlxGroup();
					page1.add(textShown);
					add(page1);
					break;
					
				case 1:
					page1.exists = page3.exists = page4.exists = page5.exists = false;
					textShown.text = content_1;
					page2 = new FlxGroup();
					page2.add(textShown);
					page2.add(dragon);
					page2.add(platform_0);
					page2.add(platform_1);
					page2.add(platform_2);
					add(page2);
					break;
					
				case 2:
					page3.exists = page1.exists = page4.exists = page5.exists = false;
					dragon.kill();
					platform_0.kill();
					platform_1.kill();
					platform_2.kill();
					textShown.y -= 10;
					textShown.text = content_2;
					page3 = new FlxGroup();
					page3.add(textShown);
					page3.add(fuz_0);
					page3.add(fuz_1);
					page3.add(fuz_2);
					add(page3);
					break;
					
				case 3:
					page2.exists = page3.exists = page1.exists = page5.exists = false;
					fuz_0.kill();
					fuz_1.kill();
					fuz_2.kill();
					textShown.text = content_3;
					textShown.y += 10;
					page4 = new FlxGroup();
					page4.add(arrow);
					page4.add(textShown);
					add(page4);
					break;
				case 4:
					page2.exists = page3.exists = page1.exists = page4.exists = false;
					arrow.kill();
					textShown.text = content_4;
					page5 = new FlxGroup();
					page5.add(textShown);
					add(page5);
					break;
				default:
					backToMenu();
			}
			
			pageCounter++;
		}
		
		override public function update():void
		{
			super.update();
			
			dragon.play("fly");
			
			arrow.velocity.x = arrow.maxVelocity.x;
			
			if (arrow.x >= FlxG.width)
				arrow.x = -arrow.width;
			
			page1.update();
			page2.update();
		}
		
		private function backToMenu():void
		{
			if (fromPause)
			{
				Registry.destroy();
				FlxG.switchState(new PlayState([], 0));
			}
			else
			{
				Registry.destroy();
				FlxG.switchState(new MenuState(true));
			}
		}
	}

}