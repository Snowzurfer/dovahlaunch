package
{
	import Collidable_Objects.CollideThings;
	import org.flixel.*;
	import org.flixel.plugin.photonstorm.*;
	
	/**
	 * Player class
	 * @author Snowzurfer
	 */
	public class Player extends FlxExtendedSprite
	{
		
		[Embed(source = "../assets/Jump3.mp3")] private var jumpSFX:Class;
		[Embed(source = "../assets/Explosion5.mp3")] private var dragonExplosionSFX:Class;
		[Embed(source = "../assets/Powerup37.mp3")] private var fuzUseSFX:Class;
		[Embed(source = "../assets/Pickup_Coin5.mp3")] private var fuzPickUpSFX:Class;
		[Embed(source = "../assets/Jump8.mp3")] private var highJumpSFX:Class;
		[Embed(source = "../assets/Hit_Hurt30.mp3")] private var arrowToTheKnwwSFX:Class;
		
		
		public var fuzPowaLevel:Number; // Charge level of the FUZ-RO-DAH
		public var firstJumpPowa:Number; // Determines how high the player jumps the first jump
		private var firstJumpPowaIncreaser:Number; // Increase the first jump power
		private var firstJumpDone:Boolean;
		private var rotationTime:int; // Determine how long the player must rotate after the FUS-RO-DAH
		private var prevRotationTime:int;
		private var rotating:Boolean; // Determine if the player must rotate at the moment, or not
		private var slying:Boolean; // Determine if the player is slaying a dragon or not
		private var arrowToTheKnee:Boolean; // Determine if the player took an arrow to the knee or not
		private var LOL:Boolean;
		
		public function Player(X:Number, Y:Number)
		{
			//	As this extends FlxSprite we need to call super() to ensure all of the parent variables we need are created
			super(X, Y);
			
			//	Load the player.png into this sprite.
			//	The 2nd parameter tells Flixel it's a sprite sheet and it should chop it up into 16x18 sized frames
			loadGraphic(Registry.playerMovementPNG, true, true, 10* 2, 8* 2, false);
			
			
			/**
			 * Setting player properties
			 */
			maxVelocity.x = 130* 2;
			maxVelocity.y = 3000* 2;
			health = 100;
			
			// Sprite size
			width = 10* 2;
			height = 8* 2;
			
			if (Registry.activePowerups != null)
			{
				fuzPowaLevel = Registry.activePowerups[0];
			}
			else 
			{
				fuzPowaLevel = 0;
			}
			firstJumpPowa = 1;
			firstJumpPowaIncreaser = 0.2;
			firstJumpDone = false;
			rotationTime = 0;
			prevRotationTime = 4;
			rotating = false;
			slying = false;
			arrowToTheKnee = false;
			
			//	The Animation sequences we need
			addAnimation("idle", [0, 0, 0, 3], 1, true);
			addAnimation("walk", [0, 1], 5, true);
			addAnimation("jump", [2], 0, true);
			addAnimation("falling", [0], 0, true);
			addAnimation("slayDragon", [2], 60, true);
			
			//	By default the sprite is facing to the right.
			//	Changing this tells Flixel to flip the sprite frames to show the left-facing ones instead.
			facing = FlxObject.RIGHT;
			
			LOL = false;
		}
		
		override public function update():void
		{
			super.update();
			
			if (firstJumpPowa > 4 || firstJumpPowa < 0)
				firstJumpPowaIncreaser = firstJumpPowaIncreaser * -1;
			
			firstJumpPowa += firstJumpPowaIncreaser;
			
			if (LOL == false && Registry.hud != null) {
				LOL = true;
				Registry.hud.changeFuzLevel(fuzPowaLevel);
			}
			
			if (!arrowToTheKnee)
			{
				// Moving onto the x assis
				if (FlxG.mouse.screenX < x) // Left
				{
					facing = FlxObject.LEFT;
					velocity.x -= maxVelocity.x;
				}
				else if (FlxG.mouse.screenX > x + 8* 2) // Right
				{
					facing = FlxObject.RIGHT;
					velocity.x += maxVelocity.x;
				}
				else
				{
					velocity.x = 0;
				}
				
				// Don't make the player go out of the screen
				if (x + 10* 2 >= FlxG.width)
					x = FlxG.width - 10* 2;
				else if (x <= 0)
					x = 0;
			}
			
			// Going up
			if (FlxG.mouse.justPressed() && isTouching(FLOOR))
			{
				// If the player bought a double first jump
				if (Registry.activePowerups[1] == "active") {
					velocity.y = (-600 * (firstJumpPowa + 1)) * 2; // The first powerful boost
					FlxG.play(highJumpSFX, 0.2);
					Registry.hud.deactivateJumpPowaBar();
				}
				// If the player bought a triple first jump
				else if (Registry.activePowerups[2] == "active") {
					velocity.y = (-600 * (firstJumpPowa + 1)) * 2; // The first powerful boost
					FlxG.play(highJumpSFX, 0.2);
					Registry.hud.deactivateJumpPowaBar();
				}
				else {
					velocity.y = (-600 * (firstJumpPowa + 1)); // The first powerful boost
					FlxG.play(highJumpSFX, 0.2);
					Registry.hud.deactivateJumpPowaBar();
				}
			}
			else if (FlxG.mouse.justPressed() && fuzPowaLevel >= 3 && !arrowToTheKnee) // The FUS-RO-DAH boost
			{
				maxVelocity.y = 1200* 2;
				velocity.y = -9000* 2;
				fuzPowaLevel = 0;
				rotating = true; // Make the player rotate
				Registry.hud.changeFuzLevel(fuzPowaLevel);
				!firstJumpDone; // Reset the first jump maxVelocity increaser flag, in order to let the player dive more for a certain time
				Registry.fx.showFuz();
				FlxG.play(fuzUseSFX, 0.3, false, true);
			}
			else
				acceleration.y = 350* 2;
			
			
			/**
			 * Animations
			 */
			if (slying)
			{
				slying = false;
				play("slayDragon");
			}
			 
			if (touching == FlxObject.FLOOR)
			{
				if (velocity.x != 0)
					play("walk");
				else
					play("idle");
			}
			else if (velocity.y < 0)
				play("jump");
			else if (velocity.y > 0)
				play("falling");
			if (rotating)
			{
				rotationTime++;
				if (rotationTime - prevRotationTime >= 4)
				{
					angle += 45;
					prevRotationTime = rotationTime;
				}
			}
			
		}
		
		// Occurs when the player hit an object
		public function playerHitObject(player:FlxObject, object:CollideThings):void
		{
			// If the player collided with a platform and his velocity is less than the one given by it
			if (object.type == "platform" && player.velocity.y > -object.jumpDist && !arrowToTheKnee)
			{
				if(!firstJumpDone) // Set the max velocity down just after to have completed the first powerful jump
				{
					maxVelocity.y = 500* 2; // Reset the maxVelocity
					!firstJumpDone; // Reset the jumpDone
					rotating = false; // Stop the player rotating if he has finished the FUS-RO-DAH
					prevRotationTime = 4; // Reset the previous rotation time
					angle = 0; // Reset the sprite angle
				}
				velocity.y = -object.jumpDist;
				FlxG.play(jumpSFX, 0.3, false, true);
				Registry.score = Registry.score.add(Registry.scoreGiven);
				Registry.scoreGiven = Registry.scoreGiven.add(5);
				object.disable();
			}
			// If the player collided with a FUS-RO-DAH powerup
			else if (object.type == "fuz" && !arrowToTheKnee)
			{
				FlxG.play(fuzPickUpSFX, 0.4, false, true);
				Registry.score = Registry.score.add(1000);
				fuzPowaLevel += 1;
				Registry.hud.changeFuzLevel(fuzPowaLevel);
				object.disable();
			}
			// If the player collided with a dragon
			else if (object.type == "dragon" && player.velocity.y > -object.jumpDist && !arrowToTheKnee)
			{
				FlxG.play(dragonExplosionSFX, 0.5, false, true);
				Registry.score = Registry.score.multiply(2);
				velocity.y = -object.jumpDist;
				object.disable();
				slying = true;
			}
			// If the player took an arrow to the knee
			else if (object.type == "arrow" && player.velocity.y > -300* 2 && !arrowToTheKnee)
			{
				arrowToTheKnee = true;
				FlxG.music.stop();
				FlxG.play(arrowToTheKnwwSFX, 0.6);
				velocity.x = 0;
				velocity.y = -object.jumpDist;
				player.angle = 180;
				object.disable();
			}
			// If the player collected a coin
			else if (object.type == "coin" && !arrowToTheKnee)
			{
				FlxG.play(fuzPickUpSFX, 0.4, false, true);
				Registry.score = Registry.score.add(10);
				Registry.coins ++;
				object.disable();
			}
		}
		
		public function setFuzPowaLevel(value:int):void 
		{
			fuzPowaLevel = value;
		}
		
		public function getFuzPowaLevel():int
		{
			return fuzPowaLevel;
		}
		
		public function getKneeStatus():Boolean
		{
			return arrowToTheKnee;
		}
	}
}