package  
{
	import org.flixel.FlxButton;
	import org.flixel.FlxGroup;
	import org.flixel.FlxState;
	import org.flixel.FlxSprite;
	import org.flixel.FlxText;
	import org.flixel.plugin.photonstorm.FlxButtonPlus;
	import org.flixel.FlxG;
	import org.flixel.plugin.photonstorm.FlxExtendedSprite;
	import FGL.GameTracker.*;
	
	/**
	 * Shows a show where to buy upgrades
	 * @author Snowzurfer
	 */
	public class ShopPage extends FlxState
	{
		// External resources
		[Embed(source = "../assets/FuzPowerUp_16.png")] private var fuzPNG:Class;
		[Embed(source = "../assets/coin.png")] private var coinPNG:Class;
		[Embed(source = "../assets/Shop_Button_0.png")] private var buttonPNG_0:Class;
		[Embed(source = "../assets/Shop_Button_1.png")] private var buttonPNG_1:Class;
		[Embed(source = "../assets/Shop_Button__Selected.png")] private var buttonPNG_Selected:Class;
		[Embed(source = "../assets/GameOva_Button_0.png")] private var button_0PNG:Class;
		[Embed(source = "../assets/GameOva_Button_1.png")] private var button_1PNG:Class;
		[Embed(source = "../assets/Buy_Button.png")] private var buyButtonPNG:Class;
		
		// General objects to display
		private var title:FlxText;
		private var availableCoinsLabel:FlxText;
		private var coinSprite:FlxSprite;
		private var newGameButton:FlxButtonPlus;
		private var mainMenuButton:FlxButtonPlus;
		private var buyButton:FlxButtonPlus;
		
		private var activePowerups:Array;
		private var selected:String;
		
		// Coins earned by the player
		private var coins:int;
		
		// Various objects to buy
		private var fuzRoDah:FlxSprite;
		private var twoXDragons:FlxText;
		private var twoXFirstJump:FlxText;
		private var threeXFirstJump:FlxText;
		private var suit_0:FlxSprite;
		private var suit_1:FlxSprite;
		private var suit_2:FlxSprite;
		private var suit_3:FlxSprite;
		
		// Descriptions for the objects
		private var objDescription:Array;
		private var objCost:Array;
		private var objButton:Array;
		private var dummybutton:FlxButtonPlus;
		
		// Groups containing all the things that have to be displayed
		private var showcase:FlxGroup // This is the main group, the one containing the shop shelves (or grid, or whateva you want to call it)
		private var genLayout:FlxGroup // This group contains the general layout items
		private var descPopUp:FlxGroup // This group will be shown when the user clicks onto an item, and it will describe it
		
		public function ShopPage(coins:int = 20) 
		{
			super();
			
			this.coins = coins;
		}
		
		override public function create():void {
			
			activePowerups = new Array(8);
			activePowerups[0] = 0;
			selected = null;
			
			// Set up the main group
			showcase = new FlxGroup(20);
			
			// Set up the general layout group
			genLayout = new FlxGroup(20);
			
			// General page layout setup
			newGameButton = new FlxButtonPlus(5 , 200, fadeBetweenScreens, ["newGame"], "New Game", 145, 23);
			newGameButton.textHighlight.setFormat("NES", 8* 2, 0xffffff, "center");
			newGameButton.textNormal.setFormat("NES", 8* 2, 0xffffff, "center");
			newGameButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			genLayout.add(newGameButton);
			
			mainMenuButton = new FlxButtonPlus(160, 200, fadeBetweenScreens, ["mainMenu"], "Main Menu", 145, 23);
			mainMenuButton.textHighlight.setFormat("NES", 8* 2, 0xffffff, "center");
			mainMenuButton.textNormal.setFormat("NES", 8*2, 0xffffff, "center");
			mainMenuButton.loadGraphic(new FlxSprite(0, 0, button_0PNG), new FlxSprite(0, 0, button_1PNG));
			genLayout.add(mainMenuButton);
			
			title = new FlxText(0, 5, FlxG.width, "Items Shop", true); // Set up the main title
			title.setFormat("NES", 8 * 4, 0xFFFFFF, "center");
			genLayout.add(title);
			
			availableCoinsLabel = new FlxText(((FlxG.width/2) - 70), (title.y + title.height), 100, "Coins: " + coins.toString(), true); // Set up the coins label
			availableCoinsLabel.setFormat("NES", 16, 0xFFFFFF, "center");
			coinSprite = new FlxSprite((availableCoinsLabel.x + availableCoinsLabel.width), availableCoinsLabel.y, coinPNG);
			genLayout.add(availableCoinsLabel);
			genLayout.add(coinSprite);
			
			buyButton = new FlxButtonPlus(coinSprite.x + 20, coinSprite.y + 1, null, null, "Buy",40,15); // Set up the buy selected item button
			buyButton.loadGraphic(new FlxSprite(0, 0, buyButtonPNG), new FlxSprite(0, 0, buyButtonPNG));
			buyButton.textNormal.setFormat("NES", 8 * 2, 0xffffff, "center");
			buyButton.textHighlight.setFormat("NES", 8* 2, 0xFF6A00, "center");
			buyButton.textHighlight.offset.x = buyButton.textNormal.offset.x = 5;
			buyButton.textHighlight.offset.y = buyButton.textNormal.offset.y = 5;
			buyButton.visible = false;
			genLayout.add(buyButton);
			
			genLayout.add(new FlxText(10,10,10));
			
			objDescription = new Array(4);
			objButton = new Array(4);
			objCost = new Array(4);
			
			
			// Set up the Fus Ro Dah powerup
			fuzRoDah = new FlxSprite(35, 80, fuzPNG);
			objButton[0] = new FlxButtonPlus(fuzRoDah.x - 9, fuzRoDah.y - 9, itemSelected, ["fuz"], null, 36, 36);
			objButton[0].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
			objButton[0].setMouseOverCallback(itemHovered, ["fuz"]);
			objButton[0].setMouseOutCallback(itemNotHovered);
			objDescription[0] = new FlxText(objButton[0].x - 32, (objButton[0].y + objButton[0].height) + 5, 100, "Cost: 5", true);
			objDescription[0].setFormat("NES", 8, 0xFFFFFF, "center");
			showcase.add(fuzRoDah);
			showcase.add(objButton[0]);
			showcase.add(objDescription[0]);
			
			// Set up the double dragon spawn powerup
			twoXDragons = new FlxText(110, 80, 100, "2x", true);
			twoXDragons.setFormat("NES", 16, 0xFF0000);
			objButton[1] = new FlxButtonPlus(twoXDragons.x - 9, twoXDragons.y - 9, itemSelected, ["2xD"], null, 36, 36);
			objButton[1].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
			objButton[1].setMouseOverCallback(itemHovered, ["2xD"]);
			objButton[1].setMouseOutCallback(itemNotHovered);
			objDescription[1] = new FlxText(objButton[1].x - 32, (objButton[1].y + objButton[1].height) + 5, 100, "Cost: 20", true);
			objDescription[1].setFormat("NES", 8, 0xFFFFFF, "center");
			showcase.add(twoXDragons);
			showcase.add(objButton[1]);
			showcase.add(objDescription[1]);
			
			// Set the double first jump powerup
			twoXFirstJump = new FlxText(190, 80, 100, "2x", true);
			twoXFirstJump.setFormat("NES", 16, 0xFFFFFF);
			objButton[2] = new FlxButtonPlus(twoXFirstJump.x - 9, twoXFirstJump.y - 9, itemSelected, ["2xJ"], null, 36, 36);
			objButton[2].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
			objButton[2].setMouseOverCallback(itemHovered, ["2xJ"]);
			objButton[2].setMouseOutCallback(itemNotHovered);
			objDescription[2] = new FlxText(objButton[2].x - 32, (objButton[2].y + objButton[2].height) + 5, 100, "Cost: 20", true);
			objDescription[2].setFormat("NES", 8, 0xFFFFFF, "center");
			showcase.add(twoXFirstJump);
			showcase.add(objButton[2]);
			showcase.add(objDescription[2]);
			
			// Set the triple first jump powerup
			threeXFirstJump = new FlxText(270, 80, 100, "3x", true);
			threeXFirstJump.setFormat("NES", 16, 0xFFFFFF);
			objButton[3] = new FlxButtonPlus(threeXFirstJump.x - 9, threeXFirstJump.y - 9, itemSelected, ["3xJ"], null, 36,36);
			objButton[3].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
			objButton[3].setMouseOverCallback(itemHovered, ["3xJ"]);
			objButton[3].setMouseOutCallback(itemNotHovered);
			objDescription[3] = new FlxText(objButton[3].x - 32, (objButton[3].y + objButton[3].height) + 5, 100, "Cost: 50", true);
			objDescription[3].setFormat("NES", 8, 0xFFFFFF, "center");
			showcase.add(threeXFirstJump);
			showcase.add(objButton[3]);
			showcase.add(objDescription[3]);
			
			add(showcase);
			add(genLayout);
		}
		
		// Works with all the items, and determines what to do depending on the item selected
		private function itemSelected(item:String):void {
			
			genLayout.members[5].visible = true; // Draw the buy button now that an item has been selected
			selected = item;
			genLayout.members[5].setOnClickCallback(buyItem, [selected]);
			
			switch(item) {
				case "3xJ":
					objButton[3].loadGraphic(new FlxSprite(0, 0, buttonPNG_Selected), new FlxSprite(0, 0, buttonPNG_Selected));
					objButton[0].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[1].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[2].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					break;
				case "2xJ":
					objButton[2].loadGraphic(new FlxSprite(0, 0, buttonPNG_Selected), new FlxSprite(0, 0, buttonPNG_Selected));
					objButton[0].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[1].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[3].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					
					break;
				case "2xD":
					objButton[1].loadGraphic(new FlxSprite(0, 0, buttonPNG_Selected), new FlxSprite(0, 0, buttonPNG_Selected));
					objButton[0].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[2].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[3].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					break;
				case "fuz":
					objButton[0].loadGraphic(new FlxSprite(0, 0, buttonPNG_Selected), new FlxSprite(0, 0, buttonPNG_Selected));
					objButton[1].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[2].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					objButton[3].loadGraphic(new FlxSprite(0, 0, buttonPNG_0), new FlxSprite(0, 0, buttonPNG_1));
					break;
			}
			
			
		}
		
		/**
		 * Shows the info for the item hovered
		 * @param	item
		 */
		private function itemHovered(item:String):void {
			
			var description:FlxText = generateInfo(item);
			
			genLayout.members[2].visible = false; // Deactivate the title label
			genLayout.members[3].visible = false; // Deactivate the coins label
			genLayout.members[4].visible = false; // Deactivate the coins sprite
			genLayout.members[5].visible = false; // Deactivate the buy button
			genLayout.members[6] = description; // Activate the description label
		}
		
		/**
		 * Remove the descrpition and restore the title and coins labels
		 */
		private function itemNotHovered():void {
			
			genLayout.members[6].visible = false; // Deactivate the description label
			genLayout.members[2].visible = true; // Activate the title label
			genLayout.members[3].visible = true; // Activate the coins label
			genLayout.members[4].visible = true; // Activate the coins sprite
			if (selected != null) {
				genLayout.members[5].visible = true; // Activate the buy button
			}
		}
		
		
		/**
		 * Generate a FlxText for showing the description of the selected powerup
		 * @param	item
		 */
		private function generateInfo(item:String):FlxText {
			
			var description:FlxText = new FlxText(10, 10, 310, null, true);
			description.setFormat("NES", 16, 0XFFFFFF, "center");
			
			switch(item) {
				case "3xJ":
					description.text = "Increase the first jump power three times";
					break;
				case "2xJ":
					description.text = "Increase the first jump power two times";
					break;
				case "2xD":
					description.text = "Increase the dragons spawn probability two times";
					break;
				case "fuz":
					description.text = "Add a FUS RO DAH powerup from the beginning of the game. Can be accumulated";
					break;
			}
			
			return description;
		}
		
		
		/**
		 * Allow the user to actually buy a powerup and subract the cost from the coins
		 * @param	item
		 */
		private function buyItem(item:String):void {
			
			switch(item) {
				case "3xJ":
					if (coins >= 50) {
						coins -= 20;
						activePowerups[2] = "active";
						coins -= 50;
						activePowerups[2] = "active";
					}
					break;
				case "2xJ":
					if(coins >= 20){
						coins -= 20;
						activePowerups[1] = "active";
					}
					break;
				case "2xD":
					if(coins >= 20){
						coins -= 20;
						activePowerups[3] = "active";
					}
					break;
				case "fuz":
					if(coins >= 5){
						coins -= 5;
						if(activePowerups[0] < 3){
							activePowerups[0] ++;
						}
					}
					break;
			}
			
			availableCoinsLabel.text = "Coins: " + coins.toString();
		}
		
		/**
		 * Shows an animation before to switch to another state
		 */
		private function fadeBetweenScreens(toWhere:String):void
		{
			//FlxG.mouse.hide();
			FlxG.flash(0xffffffff, 0.75);
			
			// If the player wnats to go to the main menu
			if(toWhere == "mainMenu"){
				FlxG.fade(0xff000000, 1, goToMainMenu);
			}
			else if(toWhere == "newGame"){
				FlxG.fade(0xff000000, 1, startGame);
			}
			
		}
		
		/**
		 * Goes to the main menu
		 */
		private function goToMainMenu():void
		{
			GameTracker.api.endGame(NaN, "ShopPage", "The player choose to go to the main menu");
			FlxG.mouse.hide();
			FlxG.switchState(new MenuState);
		}
		
		/**
		 * Starts the game
		 */
		private function startGame():void
		{
			GameTracker.api.alert("The player choose to play again", 0, "ShopPage");
			FlxG.mouse.hide();
			Registry.destroy();
			FlxG.switchState(new PlayState(activePowerups, coins));
		}
		
	}

}