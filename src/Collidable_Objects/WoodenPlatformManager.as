package Collidable_Objects 
{
	import org.flixel.*;
	
	/**
	 * Manages the platforms the player can collide with
	 * @author Snowzurfer
	 */
	public class WoodenPlatformManager extends FlxGroup
	{
		/*private var lastYCoord:int; // Y position of the last platform spawn
		private var changeScreen:Boolean; // Wheter the manager has to spawn onto a screen beside another
		private var actualSizeDifficultyLevel:int; // The difficulty level of platform size which the player is playing
		private var sizeDifficultyLevelCounter:int; // Used to determine if the difficulty level must be increased
		private var actualSpawnDistanceDifficultyLevel:int; // The difficulty level of spawn distance which the player is playing
		private var spawnDistanceDifficultyLevelCounter:int; // Used to determine if the difficulty level must be increased*/
		
		public function WoodenPlatformManager()
		{
			super();
			
			/*lastYCoord = startYCoord;
			actualSpawnDistanceDifficultyLevel = -40;
			spawnDistanceDifficultyLevelCounter = 0;
			actualSizeDifficultyLevel = 0;
			sizeDifficultyLevelCounter = 0;*/
			
			for (var j:int = 0; j < 10; j++)
			{
				var platform:WoodenPlatform = new WoodenPlatform();
				add(platform);
			}
			
			//changeScreen = true;
		}
		
		public function relase(actualSizeDifficultyLevel:int):void
		{
			var platform:WoodenPlatform = WoodenPlatform(getFirstAvailable());
			
			if (platform)
				platform.launchWooden(actualSizeDifficultyLevel);	
		}
		
		
		/*override public function update():void
		{
			super.update();
			
			// If the screen currently displayed is the first one
			if(Registry.sky1.y <= 0 && changeScreen)
			{
				if (lastYCoord - Registry.sky1.y <= actualSpawnDistanceDifficultyLevel)
				{
					lastYCoord = Registry.sky1.y;
					relase();
				}
			}
			else if (changeScreen)
			{
				lastYCoord -= Registry.sky1.height;
				changeScreen = false;
				
				// Increase the counters after 1 map scroll
				sizeDifficultyLevelCounter++;
				spawnDistanceDifficultyLevelCounter++;
				
				
			}
			
			// If the screen currently displayed is the second one
			if (Registry.sky2.y <= 0 && changeScreen == false)
			{
				if (lastYCoord - Registry.sky2.y <= actualSpawnDistanceDifficultyLevel)
				{
					lastYCoord = Registry.sky2.y;
					relase();
				}
			}
			else if (!changeScreen)
			{
				lastYCoord -= Registry.sky2.height;
				changeScreen = true;
				
				// Increase the counters after 1 map scroll
				sizeDifficultyLevelCounter++; 
				spawnDistanceDifficultyLevelCounter++;
			}
			
			// If it's time to make player life's harder
			if (sizeDifficultyLevelCounter >= 4 && actualSizeDifficultyLevel < 4)
			{
				sizeDifficultyLevelCounter = 0; // Reset the counter
				actualSizeDifficultyLevel++; // Increase the difficulty
			}
			
			// After 3 map complete scrolls, increase the difficulty
			if (actualSpawnDistanceDifficultyLevel > -110 && spawnDistanceDifficultyLevelCounter >= 2)
			{
				actualSpawnDistanceDifficultyLevel -= 2;
				spawnDistanceDifficultyLevelCounter = 0; // Reset the counter
			}
			
		}*/
	}
}