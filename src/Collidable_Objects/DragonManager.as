package Collidable_Objects
{
	import org.flixel.*;
	
	/**
	 * Manages the dragons the player can collide with
	 * @author Snowzurfer
	 */
	public class DragonManager extends FlxGroup
	{
		// Various dragon textures
		[Embed(source = "../../assets/Dragon_Sheet_Red(1).png")] private var red_dragonPNG:Class;
		[Embed(source = "../../assets/Dragon_Sheet_Purple(1).png")] private var purple_dragonPNG:Class;
		[Embed(source = "../../assets/Dragon_Sheet_Orange(1).png")] private var orange_dragonPNG:Class;
		[Embed(source = "../../assets/Dragon_Sheet_Green(1).png")] private var green_dragonPNG:Class;
		[Embed(source = "../../assets/Dragon_Sheet_Cyan(1).png")] private var cyan_dragonPNG:Class;
		[Embed(source = "../../assets/Dragon_Sheet_Senape(1).png")] private var senape_dragonPNG:Class;
		
		
		/*private var lastYCoord:int;
		private var changeScreen:Boolean;*/
		private var whatTexture:int; // Used to determine which texture the manager should use for that dragon
		private var whatPNG:Class;
		
		//The distance bewteen each object
		//private var spawnDistance:int;
		
		public function DragonManager() 
		{
			super();
			
			/*lastYCoord = startYCoord;
			spawnDistance = -40;*/
			
			for (var j:int = 0; j < 6; j++)
			{
				var dragon:Dragon = new Dragon();
				add(dragon);
			}
			
			//changeScreen = true;
		}
		
		public function relase():void
		{
			var dragon:Dragon = Dragon(getFirstAvailable());
			
			whatTexture = Math.random() * 10;
			
			// In order to not make embed every single texture in each dragon created, they are choosen by the manager
			switch(whatTexture)
			{
				case 0:
					whatPNG = red_dragonPNG;
					break;
				case 1:
					whatPNG = senape_dragonPNG;
					break;
				case 2:
					whatPNG = purple_dragonPNG;
					break;
				case 3:
					whatPNG = orange_dragonPNG;
					break;
				case 4:
					whatPNG = cyan_dragonPNG;
					break;
				case 5:
					whatPNG = green_dragonPNG;
					break;
				default:
					whatPNG = red_dragonPNG
			}
			
			if (dragon)
				dragon.launchDragon(whatPNG);	
		}
		
		
		/*override public function update():void
		{
			super.update();
			
			Math.random();
			
			// If the screen currently displayed is the first one
			if(Registry.sky1.y <= 0 && changeScreen)
			{
				if (lastYCoord - Registry.sky1.y <= spawnDistance)
				{
					lastYCoord = Registry.sky1.y;
					
					// If the player is lucky
					if (Math.random() * 100 <= 10)
					{
						relase();
					}
				}
			}
			else if (changeScreen)
			{
				lastYCoord = Registry.sky2.y;
				changeScreen = false;
				
				// After 2 map complete scrolls, increase the difficulty
				if (spawnDistance > -60)
				{
					spawnDistance -= 2;
				}
			}
			
			// If the screen currently displayed is the second one
			if (Registry.sky2.y <= 0 && changeScreen == false)
			{
				if (lastYCoord - Registry.sky2.y <= spawnDistance)
				{
					lastYCoord = Registry.sky2.y;
					
					// If the player is lucky
					if (Math.random() <= 0.1)
					{
						relase();
					}
				}
			}
			else if (!changeScreen)
			{
				lastYCoord = Registry.sky1.y;
				changeScreen = true;
				
				// After 2 map complete scrolls, increase the difficulty
				if (spawnDistance > -60)
				{
					spawnDistance -= 2;
				}
			}
		}*/
	}
}