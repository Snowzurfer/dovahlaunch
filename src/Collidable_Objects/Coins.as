package Collidable_Objects 
{
	/**
	 * Collectible coins
	 * @author Snowzurfer
	 */
	public class Coins extends CollideThings
	{
		// External resources
		[Embed(source = "../../assets/coin.png")] private var coinPNG:Class;
		
		public function Coins() 
		{
			super();
			
			// Load the graphics
			loadGraphic(coinPNG, false, false, 13, 13);
			
			// Set the kind of collidable object
			type = "coin";
			
			jumpDist = 0;
		}
		
	}

}