package Collidable_Objects 
{
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	
	/**
	 * ...
	 * @author Snowzurfer
	 */
	public class Arrow extends CollideThings
	{
		// External resources
		[Embed(source = "../../assets/arrow_to_the_knee_16.png")] private var arrowPNG:Class;
		
		public function Arrow() 
		{
			super();
			
			// How far the arrow push the player when hitten
			jumpDist = 50* 2;
			
			type = "arrow";
			
			loadGraphic(arrowPNG, false, true, 8* 2, 3* 2);
			
			maxVelocity.x = 23* 2;
			
			ySpawnDistance = 40 * 2;
		}
		
		override public function update():void
		{	
			super.update();
			
			// Pretty simple, makes the arrow "fly" around the level
			if (facing == FlxObject.RIGHT)
				velocity.x += maxVelocity.x;
			else if (facing == FlxObject.LEFT)
				velocity.x -= maxVelocity.x;
		}
		
		public function launchArrow():void
		{
			super.launch();
			
			if (x >= FlxG.width / 2) // If the arrow spawned to the left
			{
				x = FlxG.width-5* 2;
				facing = FlxObject.LEFT;
			}
			else // If the arrow spawned to the right
			{
				x = 0
				facing = FlxObject.RIGHT;
			}
		}
	}

}