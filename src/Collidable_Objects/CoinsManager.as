package Collidable_Objects 
{
	import org.flixel.FlxGroup;
	/**
	 * Manages the coins
	 * @author Snowzurfer
	 */
	public class CoinsManager extends FlxGroup
	{
		
		public function CoinsManager() 
		{
			super();
			
			for (var j:int = 0; j < 3; j++)
			{
				var coin:Coins = new Coins();
				add(coin);
			}
			//changeScreen = true;
		}
		
		
		public function relase():void
		{
			var coin:Coins = Coins(getFirstAvailable());
			
			if (coin)
				coin.launch();	
		}
		
	}

}