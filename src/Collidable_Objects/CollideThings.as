package Collidable_Objects
{
	import org.flixel.*;
	
	/**
	 * Generic class representing an object that the player can hit when
	 * diving into the sky
	 * @author Snowzurfer
	 */
	public class CollideThings extends FlxSprite
	{
		/**
		 * Represents the object texture
		 */
		protected var objectPNG:Class;
		
		/**
		 * How far the object makes the player jump when hitten
		 */
		public var jumpDist:int;
		 
		/**
		 * Tells what kind of object it is
		 */
		public var type:String;
		
		/**
		 * The distance between each object
		 */
		protected var ySpawnDistance:int;
		
		
		public function CollideThings() 
		{
			super(0, 0);
			
			ySpawnDistance = 40* 2;
			
			exists = false;
		}
		
		/**
		 * Launches the object, and determines where using the actual position of the player
		 */
		public function launch():void
		{
			x = int(Math.random() * FlxG.width);
			
			// Don't let the object spawn too much outside the borders
			if (x >= FlxG.width - 12* 2)
				x = FlxG.width - 12* 2;
			else if (x <= 2* 2)
				x = 2* 2;

			y = -ySpawnDistance;
			
			exists = true;
		}
		
		override public function update():void
		{
			super.update();
			
			// The objects must follow the background as if they were part of it
			velocity.y = Registry.sky1.velocity.y;
			
			/*if (velocity.x != 0)
			{
				velocity.x = velocity.x;
			}*/
			
			if (y > FlxG.height + 50* 2) // If the object goes off the screen
			{
				exists = false;
			}
		}
		
		// Set the "exists" property to false, play a music and show score
		public function disable():void
		{
			Registry.fx.hideScore(); // Reset score first
			if (type == "platform")
				Registry.fx.showScore(Registry.scoreGiven.toString(), new FlxPoint(x +2* 2, y + 2* 2));
			else if (type == "dragon")
			{
				Registry.fx.showScore("2X", new FlxPoint(x +2* 2, y + 2* 2));
				Registry.fx.showDragonSlayer();
				Registry.fx.emitParticles(x + 8* 2, y + 4* 2);
			}
			else if (type == "fuz")
			{
				switch(Registry.player.getFuzPowaLevel())
				{
					case 1:
						Registry.fx.showScore("FUS", new FlxPoint((FlxG.width / 2) - 6* 2, 100* 2));
						break;
					case 2:
						Registry.fx.showScore("RO", new FlxPoint((FlxG.width / 2) - 6* 2, 100* 2));
						break;
					case 3:
						Registry.fx.showScore("DA", new FlxPoint((FlxG.width / 2) - 6* 2, 100* 2));
						break;
				}
			}
			else if (type == "arrow")
			{
				
			}
			
			exists = false;
		}
	}
}