package Collidable_Objects
{
	import org.flixel.*;
	
	/**
	 * Dragon collidable object
	 * @author Snowzurfer
	 */
	
	public class Dragon extends CollideThings
	{
		/**
		 * The texture of this object
		 */
		
		
		
		public function Dragon()
		{
			super();
			
			// How far the dragon push the player when hitten
			jumpDist = 500* 2;
			
			// Set the kind of collidable object
			type = "dragon";
			
			maxVelocity.x = 20* 2;
		}
		
		override public function update():void
		{	
			super.update();
			
			// Pretty simple, makes the dragon "fly" arounf the level
			if (facing == FlxObject.RIGHT)
				velocity.x += maxVelocity.x;
			else if (facing == FlxObject.LEFT)
				velocity.x -= maxVelocity.x;
			
			if (x <= 0)
			{
				facing = FlxObject.RIGHT;
			}
			else if (x >= FlxG.width-8* 2)
			{
				facing = FlxObject.LEFT;
			}
			
			play("fly");
		}
		
		public function launchDragon(whichTexture:Class):void
		{
			loadGraphic(whichTexture, false, true, 16* 2, 9* 2, false);
			
			addAnimation("fly", [0, 1], 2, true);
		
			super.launch();
		}
		
		
		
	}

}