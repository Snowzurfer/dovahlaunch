package Collidable_Objects
{
	import org.flixel.*;
	
	/**
	 * Basic collidable platform object
	 * @author Snowzurfer
	 */
	public class WoodenPlatform extends CollideThings
	{
		// External Resource
		[Embed(source = "../../assets/platform_0(1).png")] private var platform_0PNG:Class;
		[Embed(source = "../../assets/platform_1(1).png")] private var platform_1PNG:Class;
		[Embed(source = "../../assets/platform_2(1).png")] private var platform_2PNG:Class;
		[Embed(source = "../../assets/platform_3(1).png")] private var platform_3PNG:Class;
		[Embed(source = "../../assets/platform_4(1).png")] private var platform_4PNG:Class;
		
		// Tells the class which platform it has to handle at the moment
		private var actualPlatform:int;
		
		public function WoodenPlatform() 
		{
			super();
			
			actualPlatform = 0;
			
			type = "platform";
			
			// How far the platform push the player when hitten
			jumpDist = 300* 2;
			
			ySpawnDistance = 40* 2;
		}
		
		// Allows the game to choose and increase the difficulty level
		public function launchWooden(difficultyLevel:int):void
		{
			selectGraphics(difficultyLevel);
			
			super.launch();
		}
		
		/**
		 *  Makes the game harder by decreasing the platform size
		 */ 
		public function selectGraphics(difficultyLevel:int):void
		{
			switch(difficultyLevel)
			{
				case 0:
					loadGraphic(platform_0PNG, false, false, 12* 2, 2* 2);
					break;
				case 1:
					loadGraphic(platform_1PNG, false, false, 10* 2, 2* 2);
					break;
				case 2:
					loadGraphic(platform_2PNG, false, false, 8* 2, 2* 2);
					break;
				case 3:
					loadGraphic(platform_3PNG, false, false, 6* 2, 2* 2);
					break;
				case 4:
					loadGraphic(platform_4PNG, false, false, 4* 2, 2* 2);
					break;
			}
		}
		
	}

}