package Collidable_Objects 
{
	import org.flixel.*;
	
	/**
	 * Represent a FUS-RO-DAH object
	 * @author Snowzurfer
	 */
	public class Fusrodah extends CollideThings
	{
		// External Resource
		[Embed(source="../../assets/FuzPowerUp_16.png")] private var fusrodahPNG:Class;
		
		public function Fusrodah() 
		{
			super();
			
			// Load the graphics
			loadGraphic(fusrodahPNG, false, false, 10* 2, 9* 2);
			
			// Set the kind of collidable object
			type = "fuz";
			
			jumpDist = 0;
		}
		
	}

}