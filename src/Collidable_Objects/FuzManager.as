package Collidable_Objects 
{
	import org.flixel.*;
	
	/**
	 * Manages the FUS-RO-DAH objects
	 * @author Snowzurfer
	 */
	public class FuzManager extends FlxGroup
	{
		/*private var lastYCoord:int;
		private var changeScreen:Boolean;*/
		
		//The distance bewteen each object
		//private var spawnDistance:int;
		
		public function FuzManager() 
		{
			super();
			
			/*lastYCoord = startYCoord;
			spawnDistance = -40;*/
			
			for (var j:int = 0; j < 3; j++)
			{
				var fuz:Fusrodah = new Fusrodah();
				add(fuz);
			}
			//changeScreen = true;
		}
		
		
		public function relase():void
		{
			var fuz:Fusrodah = Fusrodah(getFirstAvailable());
			
			if (fuz)
				fuz.launch();	
		}
		
		
		/*override public function update():void
		{
			super.update();
			
			Math.random();
			
			// If the screen currently displayed is the first one
			if(Registry.sky1.y <= 0 && changeScreen)
			{
				if (lastYCoord - Registry.sky1.y <= spawnDistance)
				{
					lastYCoord = Registry.sky1.y;
					
					// If the player is lucky
					if (Math.random() <= 0.09)
					{
						relase();
					}
				}
			}
			else if (changeScreen)
			{
				lastYCoord = Registry.sky2.y;
				changeScreen = false;
				
				// After 2 map complete scrolls, increase the difficulty
				if (spawnDistance > -60)
				{
					spawnDistance -= 2;
				}
			}
			
			// If the screen currently displayed is the second one
			if (Registry.sky2.y <= 0 && changeScreen == false)
			{
				if (lastYCoord - Registry.sky2.y <= spawnDistance)
				{
					lastYCoord = Registry.sky2.y;
					
					// If the player is lucky
					if (Math.random() <= 0.09)
					{
						relase();
					}
				}
			}
			else if (!changeScreen)
			{
				lastYCoord = Registry.sky1.y;
				changeScreen = true;
				
				// After 2 map complete scrolls, increase the difficulty
				if (spawnDistance > -60)
				{
					spawnDistance -= 2;
				}
			}
		}*/
	}

}