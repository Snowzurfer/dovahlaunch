package Collidable_Objects 
{
	import org.flixel.FlxGroup;
	
	
	/**
	 * Manages all the managers for all the objects
	 * * @author Snowzurfer
	 */
	public class ObjectsManager extends FlxGroup
	{
		private var actualSkyYPos:Number; // Contains the position, at each frame, of the map currently displayied
		
		private var changeScreen:Boolean; // Wheter the manager has to spawn onto a screen beside another
		
		// -Platforms management related counters--------------------------------------------------------------------------------
		private var platformLastYCoord:int; // Y position of the last object spawn
		private var actualSizeDifficultyLevel:int; // The difficulty level of platform size which the player is playing
		private var sizeDifficultyLevelCounter:int; // Used to determine if the difficulty level must be increased
		private var actualSpawnDistanceDifficultyLevel:int; // The difficulty level of spawn distance which the player is playing
		private var spawnDistanceDifficultyLevelCounter:int; // Used to determine if the difficulty level must be increased
		// -----------------------------------------------------------------------------------------------------------------------
		
		// -Fuz management related counters --------------------------------------------------------------------------------------
		private var fuzSpawnDistance:int; // Distance between each fuz powerup
		private var fuzActualSpawnDistance:int; // Actual distance, used to spawn them
		private var fuzSpawnProb:Number; // Probability of spawning a fuz poerwup
		private var fuzDifficultyIncreaserCounter:int; // Counts how many scrolls the screens have done
		// -----------------------------------------------------------------------------------------------------------------------
		
		// -Dragon management related counters -----------------------------------------------------------------------------------
		private var dragonSpawnDistance:int; // Distance between each dragon
		private var dragonActualSpawnDistance:int; // Actual distance, used to spawn them
		private var dragonSpawnProb:Number; // Probability of spawning a dragon
		private var dragonDifficultyIncreaserCounter:int; // Counts how many scrolls the screens have done
		// -----------------------------------------------------------------------------------------------------------------------
		
		// -Arrows management related counters -----------------------------------------------------------------------------------
		private var arrowSpawnDistance:int; // Distance between each arrow
		private var arrowActualSpawnDistance:int; // Actual distance, used to spawn them
		private var arrowSpawnProb:Number; // Probability of spawning an arrow
		// -----------------------------------------------------------------------------------------------------------------------
		
		// -Coins management related counters --------------------------------------------------------------------------------------
		private var coinSpawnDistance:int; // Distance between each coin
		private var coinsActualSpawnDistance:int; // Actual distance, used to spawn them
		private var coinsSpawnProb:Number; // Probability of spawning a coin
		// -----------------------------------------------------------------------------------------------------------------------
		
		// Platforms manager
		private var platformsManager:WoodenPlatformManager;
		
		// FUS-RO-DAH manager
		private var fuzManager:FuzManager;
		
		// Dragons manager
		private var dragonManager:DragonManager;
		
		// Arrows manager
		private var arrowsManager:ArrowsManager;
		
		// Coins manager
		private var coinsManager:CoinsManager;
		
		
		public function ObjectsManager(startYCoord:int) 
		{
			platformLastYCoord = startYCoord;
			changeScreen = true;
			
			actualSpawnDistanceDifficultyLevel = -40* 2;
			spawnDistanceDifficultyLevelCounter = 0;
			actualSizeDifficultyLevel = 0;
			sizeDifficultyLevelCounter = 0;
			
			dragonSpawnDistance = -120* 2;
			dragonActualSpawnDistance = startYCoord;
			dragonSpawnProb = 0.20;
			dragonDifficultyIncreaserCounter = 0;
			
			fuzSpawnDistance = -100* 2;
			fuzActualSpawnDistance = startYCoord;
			fuzSpawnProb = 0.25;
			fuzDifficultyIncreaserCounter = 0;
			
			arrowSpawnDistance = -200* 2;
			arrowActualSpawnDistance = startYCoord;
			arrowSpawnProb = 0.10;
			
			coinSpawnDistance = -200;
			coinsActualSpawnDistance = startYCoord;
			coinsSpawnProb = 0.70;
			
			platformsManager = new WoodenPlatformManager();
			dragonManager = new DragonManager();
			fuzManager = new FuzManager();
			arrowsManager = new ArrowsManager();
			coinsManager = new CoinsManager();
			
			
			
			add(platformsManager);
			add(dragonManager);
			add(fuzManager);
			add(arrowsManager);
			add(coinsManager);
		}
		
	
		override public function update():void
		{
			super.update();
			
			
			// The Y sky position is accessed only once per frame, in order to not use too much CPU
			if (changeScreen)
				actualSkyYPos = Registry.sky1.y; 
			else
				actualSkyYPos = Registry.sky2.y;
			
				
			// If the screen currently displayed is the first one
			if(actualSkyYPos <= 0 && changeScreen)
			{
				// If it's time to spawn a platform
				if (platformLastYCoord - actualSkyYPos <= actualSpawnDistanceDifficultyLevel)
				{
					platformLastYCoord = actualSkyYPos;
					platformsManager.relase(actualSizeDifficultyLevel); // Relase a platform
				}
				
				// If it's time to spawn a dragon
				if (dragonActualSpawnDistance - actualSkyYPos <= dragonSpawnDistance)
				{
					dragonActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= dragonSpawnProb)
					{
						dragonManager.relase();
					}
				}
				
				// If it's time to spawn a fuz powerup
				if (fuzActualSpawnDistance - actualSkyYPos <= fuzSpawnDistance)
				{
					fuzActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= fuzSpawnProb)
					{
						fuzManager.relase();
					}
				}
				
				// If it's time to spawn an arrow
				if (arrowActualSpawnDistance - actualSkyYPos <= arrowSpawnDistance)
				{
					arrowActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= fuzSpawnProb)
					{
						arrowsManager.relase();
					}
				}
				
				// If it's time to spawn a coin
				if (coinsActualSpawnDistance - actualSkyYPos <= coinSpawnDistance) {
					
					coinsActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= coinsSpawnProb)
					{
						coinsManager.relase();
					}
				}
			}
			// If the fisrt screen has finished to scroll
			else if (changeScreen)
			{
				platformLastYCoord -= Registry.sky1.height;
				dragonActualSpawnDistance -= Registry.sky1.height
				fuzActualSpawnDistance -= Registry.sky1.height;
				arrowActualSpawnDistance -= Registry.sky1.height;
				coinsActualSpawnDistance -= Registry.sky1.height;
				actualSkyYPos = Registry.sky2.y;
				changeScreen = false;
				
				// Increase the counters after 1 map scroll
				sizeDifficultyLevelCounter++;
				spawnDistanceDifficultyLevelCounter++;
				dragonDifficultyIncreaserCounter++;
				fuzDifficultyIncreaserCounter++;
			}
			
			
			// If the screen currently displayed is the second one
			if (actualSkyYPos <= 0 && changeScreen == false)
			{
				// If it's time to spawn a platform
				if (platformLastYCoord - actualSkyYPos <= actualSpawnDistanceDifficultyLevel)
				{
					platformLastYCoord = actualSkyYPos;
					platformsManager.relase(actualSizeDifficultyLevel); // Relase a platform
				}
				
				// If it's time to spawn a dragon
				if (dragonActualSpawnDistance - actualSkyYPos <= dragonSpawnDistance)
				{
					dragonActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= dragonSpawnProb)
					{
						dragonManager.relase(); // Relase a dragon
					}
				}
				
				// If it's time to spawn a fuz powerup
				if (fuzActualSpawnDistance - actualSkyYPos <= fuzSpawnDistance)
				{
					fuzActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= fuzSpawnProb)
					{
						fuzManager.relase();
					}
				}
				
				// If it's time to spawn an arrow
				if (arrowActualSpawnDistance - actualSkyYPos <= arrowSpawnDistance)
				{
					arrowActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= fuzSpawnProb)
					{
						arrowsManager.relase();
					}
				}
				
				// If it's time to spawn a coin
				if (coinsActualSpawnDistance - actualSkyYPos <= coinSpawnDistance) {
					
					coinsActualSpawnDistance = actualSkyYPos;
					
					// If the player is lucky
					if (Math.random() <= coinsSpawnProb)
					{
						coinsManager.relase();
					}
				}
			}
			// If the second screen has finished to scroll
			else if (!changeScreen)
			{
				platformLastYCoord -= Registry.sky2.height;
				dragonActualSpawnDistance -= Registry.sky2.height
				fuzActualSpawnDistance -= Registry.sky2.height;
				arrowActualSpawnDistance -= Registry.sky2.height;
				coinsActualSpawnDistance -= Registry.sky2.height;
				actualSkyYPos = Registry.sky1.y; 
				changeScreen = true;
				
				// Increase the counters after 1 map scroll
				sizeDifficultyLevelCounter++; 
				spawnDistanceDifficultyLevelCounter++;
				dragonDifficultyIncreaserCounter++;
				fuzDifficultyIncreaserCounter++;
			}
			
			// If it's time to decrease platforms size
			if (sizeDifficultyLevelCounter >= 6 && actualSizeDifficultyLevel < 4)
			{
				actualSizeDifficultyLevel++; // Increase difficulty
				sizeDifficultyLevelCounter = 0; // Reset the counter
			}
			
			// After 3 map complete scrolls, increase the platforms spawn distance
			if (actualSpawnDistanceDifficultyLevel > -110 && spawnDistanceDifficultyLevelCounter >= 3)
			{
				actualSpawnDistanceDifficultyLevel -= 2; // Increase difficulty
				spawnDistanceDifficultyLevelCounter = 0; // Reset the counter
			}
			
			// After 4 map complete scrolls, decresase probability of spawning a dragon
			if (dragonDifficultyIncreaserCounter >= 4 && dragonSpawnProb >= 0.05)
			{
				dragonSpawnProb -= 0.05; // Increase difficulty
				dragonDifficultyIncreaserCounter = 0; // Reset the counter
			}
			
			// After 2 map complete scrolls, decresase probability of spawning a fuz powerup
			if (fuzDifficultyIncreaserCounter >= 2 && fuzSpawnProb >= 0.01)
			{
				fuzSpawnProb -= 0.01; // Increase difficulty
				fuzDifficultyIncreaserCounter = 0; // Reset the counter
			}
		}
	}
}